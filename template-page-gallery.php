<?php

// Template Name: Pagina com Galeria

get_header();

$options = get_option('page_1_section_option');



if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner <?php if(strlen(get_the_title()) > 39) echo ' small-font'; ?>"><span><?php the_title(); ?></span></h1>

                <h2 class="text-uppercase">Nossos produtos passam por rigorosos testes para apresentar a melhor qualidade para você</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-md-6">

                    <article class="artigo" id="sobre">

                        <?php the_content(); ?>

                        <div class="callToAction marginT62">

                            <button class="btn btn-primary inverted" data-toggle="modal" data-target="#modal-duvidas">Dúvidas? Nós te ligamos</button>

                        </div>

                    </article>

                </div>



                <aside id="sidebar-produto" class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">

                    <?php $get = unserialize( get_post_meta($post->ID, 'galeria', true ) );



                    if(!empty($get)):?>

                        <!-- Galeria -->

                        <div id="galeria" class="row marginT30">

                            <div class="big owl-carousel" id="sync1">

                                <?php foreach($get as $key => $id_img):

                                    $fullImage      = wp_get_attachment_image_src( $id_img, 'full' );

                                    $mainThumbImg   = wp_get_attachment_image_src( $id_img, 'galeria-produto' );

                                    $mainAlt        = get_post_meta( $id_img, '_wp_attachment_image_alt' );

                                    if( !empty( $mainAlt ) ) : $alt = $mainAlt[0];

                                        else: $alt = 'Imagem sem texto alternativo';

                                    endif; ?>



                                    <div class="item">

                                        <a href="<?= $fullImage[0] ?>" data-lightbox="galeria-produto">

                                            <img src="<?php echo $mainThumbImg[0]; ?>" class="img-responsive" title="<?php echo $alt; ?>" alt="<?php echo $alt; ?>"/>

                                        </a>

                                    </div>

                                <?php endforeach; ?>

                            </div>



                            <div class="thumbs" id="sync2">

                                <?php foreach($get as $key => $id_img):

                                    $thumbImg = wp_get_attachment_image_src( $id_img, 'thumb-galeria-produto' ); ?>

                                    <div class="thumb item <?php if($key == 0) echo ' active'; ?>" style="background: url('<?php echo $thumbImg[0]; ?>') no-repeat center center; background-size: cover;"></div>

                                <?php endforeach; ?>

                            </div>



                        </div>

                    <?php endif; ?>



                    <?php require('inc/form-catalogo.php'); ?>



                </aside>

            </div>



        </div>

    </section>

<?php endwhile; endif; ?>



    <!-- Modal -->

    <div class="modal fade" id="modal-duvidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <h4 class="modal-title" id="myModalLabel">Dúvidas? Nós te ligamos!</h4>

                </div>

                <form id="callme" action="<?php echo get_stylesheet_directory_uri(); ?>/inc/envia.php" method="POST">

                    <input type="hidden" name="tipo" value="callme" />

                    <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">



                    <input type="hidden" name="produto" value="<?= the_title(); ?>">



                    <div class="modal-body">

                        <div class="form-group">

                            <label for="nome" class="sr-only">Nome:</label>

                            <input type="text" placeholder="Nome" name="nome" id="nome" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="email" class="sr-only">Email:</label>

                            <input type="mail" placeholder="Email" name="email" id="email" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="telefone" class="sr-only">Telefone:</label>

                            <input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="form-control celular-input" >

                        </div>



                        <div class="form-group">

                            <label for="mensagem" class="sr-only">Mensagem:</label>

                            <textarea name="mensagem" placeholder="Mensagem" id="mensagem" class="form-control"></textarea>

                        </div>



                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Me ligue!</button>

                    </div>



                </form>

            </div>

        </div>

    </div>

<?php get_footer(); ?>