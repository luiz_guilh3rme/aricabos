<?php

global $showFooter;



if($showFooter): ?>

<section id="blog" class="container marginT62">



    <div class="col-lg-7 col-md-7">

        <div id="blog-header">

            <h3 class="titulo">Blog</h3>

            <p class="marginT30">Mudanças no mercado, novas tendências, exemplos de construções, modelos de planejamento. Todos os assuntos que envolva a eficiência de uma boa obra você pode conferir na página do nosso Blog. Venha se atualizar conosco e aprender novos conceitos.</p>

        </div>



        <?php



        query_posts(

            array(

                'posts_per_page' => 2,

                'post_type'      => 'blog'

            ));



            if ( have_posts() ) : while ( have_posts() ) : the_post();?>

            <div class="row post">

                <aside id="social-blog" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">

                    <div class="post-social">

                        <div class="date"><?php the_time('d'); ?><br> <?php the_time('M'); ?></div>

                        <div class="like">LIKE</div>

                        <div class="icons">

                            <a target="_blank" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php the_permalink() ?>&p[images][0]=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>&p[title]=<?php the_title() ?>&p[summary]=<?php the_excerpt() ?>"><i class="fa fa-facebook"></i></a>

                            <a target="_blank" href="https://twitter.com/share?url=<?php the_permalink() ?>&text=<?php the_title() ?>"><i class="fa fa-twitter"></i></a>

                            <a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>&url=<?php the_permalink() ?>&description=<?php the_title() ?>"><i class="fa fa-pinterest"></i></a>

                            <a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink() ?>"><i class="fa fa-google-plus"></i></a>

                            <a target="_blank" href="http://www.linkedin.com/shareArticle?url=<?php the_permalink() ?>&title=<?php the_title() ?>"><i class="fa fa-linkedin"></i></a>

                        </div>

                    </div>

                </aside>



                <div id="blog-content" class="col-lg-10 col-md-10 col-sm-10">

                    <article>

                        <?php if(has_post_thumbnail()) the_post_thumbnail('post-thumb', array('class'=>' img-responsive thumb')); ?>

                        <a href="<?php the_permalink(); ?>"><h4 class="titulo-blog"><?php the_title(); ?></h4></a>

                        <i class="fa fa-comments"></i> <span><?php comments_number( 'Nenhum comentário', '1 Comentário', '% Comentários' ); ?></span>

                        <p><?php the_excerpt(); ?></p>

                    </article>

                </div>

            </div>

        <?php endwhile; endif; ?>



    </div>



    <aside id="blog-sidebar" class="col-lg-offset-1 col-lg-4 col-md-4">



        <div class="row">

            <h3 class="titulo">Newsletter</h3>

            <p>A Aricabos é uma empresa que tem como premissa a inovação na qualidade de seus produtos. Sabia mais mandando um e-mail para nós e avance conosco.</p>

            <form id="frmNewsletter" method="post" name="frmNewsletter" class="marginT30" data-modal="frmNewsletter">

                <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

                <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" required >

                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required>

                <button type="submit" class="btn btn-primary marginT30 pull-right" onclick="ga('send', 'event', 'Formulário', 'Cadastro', 'Newsletter');">Enviar</button>

            </form>

        </div>

    </aside>

</section>

<?php endif; ?>



<footer>

    <div class="cidade"></div>
    <style>
    .modal-bg {
        position: fixed;
        background-color: #271f4c;
        height: 100%;
        width: 100%;
        display: block;
        margin:  auto;
        left:  0;
        top:  0;
        transition: all 0.2s ease-in-out;
        text-align: center;
        overflow: hidden;
        z-index:  10000;
        display: none;
    }

    .modal-content-promotional {
        position: relative;
        top: 35%;
        transform: translateY(-50%);
        right: -100px;
        max-width: 100%;
    }

    .close-modal-content {
        color:  white;
        text-shadow: rgba(0, 0, 0, .5)  4px 8px 4px;
        position: absolute;
        top: 100px;
        right:  20%;
        z-index: 2;
        background-color: transparent;
        border: 0;
        font-size: 40px; 
        transition: color 0.2s ease-in-out;
        transform:  rotate(45deg);
    }

    .close-modal-content:hover {
        color:  #eeb313;
    }

    .download-catalog-button {
        display: block;
        position: absolute;
        top: 77%;
        transform: translateY(-50%);
        left: 0;
        right: 0;
        margin:  auto;
        width: 210px;
    }

    .download-catalog-button:hover img {
        transform: scale(1.05);
    }

    .download-catalog-button img {
        border-radius:  4px;
        transition: transform 0.2s ease-in-out;
    }

    @media (min-width: 1000px) and (max-width: 1400px) {
        .download-catalog-button {
            top: 91vh;
        }

        .modal-content-promotional {
            top:  38%;
        }

        .close-modal-content {
            top: 30px;
        }
    }

    @media (max-width: 900px) {
        .modal-content-promotional {
            left: 7vh;
            right: 0;
            top: 30vh;
        }

        .close-modal-content {
            top: 25px;
            right: 25px;
        }

        .modal-bg {
            width: 100%; 
            height: 100%;
            top:  -48px;
        }

        .download-catalog-button {
            top: 65vh;
        }
    }
</style>
<div class="modal-bg">
    <button class="close-modal-content">+</button>
    <img src="<?php bloginfo('template_url') ?>/img/modal.png" class="modal-content-promotional" alt="50 anos inovando e oferecendo equipamentos para movimentação de cargas">
    <a href="<?php echo site_url('/downloads/'); ?>" class="download-catalog-button" title="Clique aqui para baixar nosso catálogo de produtos">
        <img src="<?php bloginfo('template_url') ?>/img/catalogo.jpg" alt="BAIXAR CATÁLOGO">
    </a>
</div>

<section id="links-rodape">

    <div class="container">

        <div class="col-lg-2 col-md-2 col-sm-6">

            <h3 class="titulo-branco">Páginas Principais</h3>

            <?php

            wp_nav_menu( array(

                'menu'              => 'footer-1',

                'theme_location'    => 'footer-1',

                'depth'             => 1,

                'container'         => '',

                'container_class'   => '',

                'container_id'      => '',

                'menu_class'        => 'col-lg-12',

                'fallback_cb'       => 'wp_page_menu',

                'walker'            => ''

            )

            );?>



        </div>

        <div class="col-lg-3 col-md-3 col-sm-6">

            <h3 class="titulo-branco">Produtos</h3>

            <ul>



                <?php



                $blogCatId = get_category_by_slug('blog');



                $args = array(

                    'orderby'                   => 'name',

                    'parent'                    => 0,

                    'exclude'                   => $blogCatId->term_id,

                    'order'                     => 'ASC',

                    'taxonomy'                  => 'category',

                );



                $categories = get_categories( $args );

                foreach($categories as $category): ?>

                <li>

                    <a href="<?= get_category_link($category->term_id); ?>"><?= $category->name ?></a>

                </li>

            <?php endforeach; ?>

        </ul>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-6">

        <h3 class="titulo-branco">Aplicações</h3>

        <?php

        $tags = get_terms('post_tag', array('hide_empty'=> false));

        if($tags): ?>

        <ul>

            <?php

            foreach ($tags as $tag) :

                $id         = $tag ->term_id;

                $title      = $tag ->name; ?>

                <li>

                    <a href="<?= get_tag_link( $id ) ?>"><?= $title ?></a>

                </li>

            <?php endforeach; ?>

        </ul>

    <?php endif; ?>

</div>

<div class="col-lg-3 col-md-3" itemscope="" itemtype="http://schema.org/Organization">

    <link itemprop="url" href="<?= site_url() ?>">

    <img itemprop="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/aricabos-logo.png" alt="Aricabos logo" />

    <p itemprop="description">Empresa com 47 anos no mercado, especialista no mercado de elevação, movimentação e amarração de cargas, oferecendo produtos de excelência.</p>

    <div itemscope="" itemtype="http://schema.org/LocalBusiness">

        <meta itemprop="name" content="Aricabos">



        <div class="endereco" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"><i class="fa fa-map-marker"></i> <span itemprop="streetAddress">Av. Henry Ford n° 2257, Parque da Mooca <span itemprop="addressLocality">São Paulo</span> / <span itemprop="addressRegion">SP</span></span></div>

        <div class="phone"><i class="fa fa-phone"></i> Grande São Paulo <a class="tel-sp" href="tel:+551126027222"><span itemprop="telephone" class="grandesp">(11) 2602-7222</span></a></div>

        <div class="phone"><i class="fa fa-phone"></i> Outros Locais <a class="tel-outras-regioes" href="tel:0800-777-2226"><span itemprop="telephone" class="outraslocalidades">0800-777-2226</span></a></div>

        <div class="email"><i class="fa fa-envelope"></i><a href="mailto:vendas@aricabos.com.br">vendas@aricabos.com.br</a></div>

    </div>

    <div class="social-links">

        <a itemprop="sameAs" href="https://www.facebook.com/pages/Ari-Cabos-Ind%C3%BAstria-Com%C3%A9rcio-Importa%C3%A7%C3%A3o-e-Exporta%C3%A7%C3%A3o-Ltda/240100239448071"><i class="fa fa-facebook"></i></a>

        <a itemprop="sameAs" href="https://twitter.com/aricabos"><i class="fa fa-twitter"></i></a>

        <a itemprop="sameAs" href="http://br.linkedin.com/pub/ari-cabos/62/a51/477"><i class="fa fa-linkedin"></i></a>

<!--         <a class="pull-right logo-iso-9001" href="<?/*php echo get_stylesheet_directory_uri(); */?>/inc/download/Certificado-ISO-9001.pdf" target="_blank">

            <img src="<?php/* echo get_stylesheet_directory_uri(); */?>/img/iso-9001.svg" >

            Empresa Certificada <span class="help-block">ISO 9001</span>

        </a> -->

    </div>

</div>

</div>

</section>

<section id="copyright">

    <div class="container">

        <div class="pull-left uppercase">Copyright © 2016 aricabos</div>

        <div class="pull-right light uppercase"><a href="https://www.3xceler.com.br/criacao-de-sites/" class="link-branco">Criação de sites</a>: Agência 3xceler</div>

    </div>

</section>

</footer>



</div><!-- wrapper -->

</div><!-- main-container -->

    <?php // Menu with class outer-nav left vertical

    wp_nav_menu( array(

        'menu'              => 'primary',

        'theme_location'    => 'primary',

        'depth'             => 2,

        'menu_class'        => 'outer-nav left vertical',

        'fallback_cb'       => 'wp_page_menu',

        'walker'            => ''

        ));?>

    </div><!-- Perspective Div -->
    <?php
        get_template_part("includes/whatsapp-cta"); 
        get_template_part("includes/call-cta");
        get_template_part("includes/call-cta-forms");
    ?>


    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripts.js"></script>

<!--<script type="text/javascript">

    (function(a,e,c,f,g,b,d){var h={ak:"940066704",cl:"3gFjCPm5kGQQkI-hwAM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");

</script>-->

</body>

</html>

