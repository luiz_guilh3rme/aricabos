<div class="whatsapp-cta">
	<div class="whatsapp-tooltip">
		<i class="fa fa-whatsapp"></i>
		<p class="whatsapp-tooltip-text">Entre em contato conosco enviando uma mensagem por WhatsApp</p>			
		<span class="close-whatsapp-message">&times;</span>
	</div>
		
	<!-- <a href="#" class="btn-whatsapp-mobile"><i class="fab fa-whatsapp"></i></a> -->
	<a href="wpp" rel="nofollow noopener" target="_blank" title="Fale conosco no Whatsapp" class="wpp-lateral" onclick="ga('gtm1.send', 'event', 'Whatsapp', 'click', 'Lateral')">
		<span class="whatsapp-cta-text btn btn-pink mx-4">WHATSAPP</span>
		<img src="<?php bloginfo("template_url")?>/img/whatsapp.png" alt="Envie-nos um whatsapp!" title="Envie-nos um whatsapp!" class="whatsapp-cta-img" />
	</a>

	<a href="wpp" rel="nofollow noopener" target="_blank" title="Fale conosco no Whatsapp" class="wpp-lateral wpp-mobile" onclick="ga('gtm1.send', 'event', 'Whatsapp', 'click', 'Mobile Footer')">
		<span class="whatsapp-cta-text btn btn-pink mx-4">WHATSAPP</span>
		<img src="<?php bloginfo("template_url")?>/img/whatsapp.png" alt="Envie-nos um whatsapp!" title="Envie-nos um whatsapp!" class="whatsapp-cta-img" />
	</a>
	

	<a href="#" class="btn btn-pink open-cta">ENTRE EM CONTATO</a>
</div>