<div class="overlay">
 <button class="close-modal" aria-label="Fechar Modal">&times;</button>
    <div class="form-wrapper-all">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers" data-instance="01">
                <i class="fa fa-clock-o alt"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers active" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance" data-instance="00">
            <?php echo do_shortcode("[contact-form-7 id='2137' title='Modal - Formulário Nós te ligamos' html_class='leave-message']"); ?>
        </div>
        <div class="instance" data-instance="01">
            <?php echo do_shortcode("[contact-form-7 id='2141' title='Modal - Formulário Me ligue Depois' html_class='leave-message']"); ?>
        </div>
        <div class="instance active" data-instance="02">
            <?php echo do_shortcode("[contact-form-7 id='2144' title='Modal - Deixe uma Mensagem' html_class='leave-message']"); ?>
        </div>    
    </div>
 </div>