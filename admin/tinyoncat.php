<?php
/*
    TinyMCE Editor on Cat
    Author: Lucas Farina
    Corporation: Agency 3xceler
*/

// Remove Filtros HTML
remove_filter( 'pre_term_description', 'wp_filter_kses' );
remove_filter( 'term_description', 'wp_kses_data' );

// Remove DescriptionBOX antiga
//add_action( 'admin_print_styles', 'categorytinymce_admin_head' );
function categorytinymce_admin_head() {
    global $current_screen;
    if ( $current_screen->id == 'edit-category' OR 'edit-tag' ) { ?>
        <style type="text/css">
            .taxonomy-category .quicktags-toolbar input{float:left !important; width:auto !important;}
            .taxonomy-category [for="description"],
            .taxonomy-category textarea#description  {display:none!important;}
        </style><?php
    }
}

// Add New DescriptionBOX
//define('description1', 'Category_Description_option');

//add_filter('edit_category_form_fields', 'description1');
function description1($tag) {
    $tag_extra_fields = get_option(description1); ?>

    <table class="form-table">
    <tr class="form-field">
        <th scope="row" valign="top">
            <label for="description"><?php _ex('Description', 'Taxonomy Description'); ?></label>
        </th>

        <td> <?php
            $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description' );
            wp_editor(html_entity_decode($tag->description , ENT_QUOTES, 'UTF-8'), 'description1', $settings); ?>
        </td>
    </tr>

    </table><?php

}
?>