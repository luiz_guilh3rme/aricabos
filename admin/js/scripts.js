var $j = jQuery, oldVal;

$j(function () {
    oldVal = $j('#tabelas-select').val();

    if ($j('#galeria_pagina_list').length){

        var galeria = wp.media.frames.file_frame = wp.media({
            title: 'Selecione uma Imagem',
            button: {
                text: 'Inserir'
            },
            multiple: false
        });

        galeria.on('select', function() {
            attchGaleria = galeria.state().get('selection').first().toJSON();

            url = attchGaleria.url;
            id = attchGaleria.id;


            var current = $j('.current-item');
            if(current[0].id == 'imageAdder'){
                var fastHTML = $j('#fastTemplate').html();
                var template = Handlebars.compile(fastHTML);
                var semiObject = {
                    style: 'background: url('+url+') no-repeat center top',
                    imageId: id
                };
                var html    = template(semiObject);

                $j(html).appendTo('#galeria_pagina_list');

                current.removeClass('current-item');
            }
            else{
                current.find('.output-imagem').attr('style', "background: url('"+url+"') no-repeat center top");
                current.find('.imagem-id').val(id);
                current.removeClass('current-item');
            }
        });

        $j('body').on('click', '.b-upload', function(e){
            e.preventDefault();
            $j(this).closest('li').addClass('current-item');
            galeria.open();
        });

        // Remove campos para galeria dinamica
        $j('body').on('click', '.b-remover, .b-remover-category', function(e) {
            e.preventDefault();
            $j(this).closest('li').remove();
        });

        $j('#imageAdder').on('click', function (e) {
            e.preventDefault();

            $j(this).addClass('current-item');
            galeria.open();
        });

    }

    if($j('#variacoes').length){

        $j(document).on('click','#adicionar-variacao', function (e) {
            e.preventDefault();
            var currentTable = $j('#tabelas-select').val(), currentSelect;

            var count = $j('#variacoes li').length;

            // Get Fields
            //OLD var json = $j('#'+currentTable).find('[data-type="campos"]').val();

            var json = $j('#tabelas-select')
                .find('option[value="'+ currentTable +'"]')
                .data('campos');

            json = json.replace(new RegExp("'", 'g'), '"');

            var fields = JSON.parse(json);
            //var fields = fieldsName.split(',');

            // Dados
            var data = {
                campos: fields,
                count: count
            };

            var source = $j("#template").html();
            var template = Handlebars.compile(source);
            var html = template(data);

            $j('.variacoes').append(html);

            var input = $j('.variacoes li:last-child input[type="text"]');
            $j(input[0]).focus();

        });
        $j(document).on('click', '.remove', function (e) {
            $j(this).closest('li').remove();
        });

        // Select
        var $eventSelect = $j("#tabelas-select");

        $eventSelect.on('select2:select', function (evt) {
            var newVal = evt.params.data.id;

            if($j('#variacoes li').length) {

                if(!confirm('Você tem certeza disso ?, Se continuar todas as variações a baixo serão apagadas e substituidas por um novo modelo de tabela.')){
                    $j(this).val(oldVal).trigger('change');
                    return;
                }

            }


            $j(this).val(newVal).trigger("change");

            // Del All & Update
            oldVal = newVal;
            $j('#variacoes li').remove();

        });

    }

    if($j('#galeria_categoria').length) {
        var galeria = wp.media.frames.file_frame = wp.media({
            title: 'Selecione uma Imagem',
            button: {
                text: 'Inserir'
            },
            multiple: false
        });
        galeria.on('select', function() {
            attchGaleria = galeria.state().get('selection').first().toJSON();

            url = attchGaleria.url;
            id = attchGaleria.id;

            var current = $j('.galeria').find('.current-item');

            current.find('.output-imagem').attr('style', "background-image: url('"+url+"');");
            current.find('.imagem-id').val(id);

            current.removeClass('current-item');
        });

        $j('body').on('click', '.b-upload', function(e){
            e.preventDefault();
            $j(this).closest('li').addClass('current-item');
            galeria.open();
        });

        // Remove campos para galeria dinamica
        $j('body').on('click', '.b-remover, .b-remover-category', function(e) {
            e.preventDefault();
            $j(this).closest('li').find('.imagem-id').val('');
            $j(this).closest('li').find('.output-imagem').attr('style', 'background-image: url(http://placehold.it/290x274/D7D7D7/8F8F8F&amp;text=Selecione)');
        });
    }

});
