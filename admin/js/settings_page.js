$j = jQuery;
$j(function(){
    if($j('#tabelas').length) {

        $j(document).on('confirmation', '.remodal', function () {

            var _lastId = $j('#hidden-tables li').last().attr('id').replace('tabela_', '');
            _lastId = parseInt(_lastId);

            var count = _lastId + 1,
                nome = $j('#table-name'),
                campos = $j('#new-fields'),
                existentTableId = $j(this).attr('id');


            if(existentTableId) {
                var table = $j('#'+existentTableId);
                table.find('[data-type="nome"]').val(nome.val());
                table.find('[data-type="campos"]').val(campos.val());

                $j('#tabelas-select option[value="'+existentTableId+'"]').text(nome.val());

            }else{
                var data = {
                    count: count,
                    nome: nome.val(),
                    campos: campos.val()
                };

                var sourceTemplate  = $j('#template').html();
                var template        = Handlebars.compile(sourceTemplate);
                var html            = template(data);

                $j('#hidden-tables').append(html);
                $j('#tabelas-select').append('<option value="tabela_'+ count +'">'+ nome.val() +'</option>');
            }
        });

        $j(document).on('closed', '.remodal', function () {
            $j(this).attr('id','');
            $j('#table-name').val('');
            $j('#new-fields').val('');
            $j('.remodal h1').text('Criar Tabela');

        });

        $j(document).on('change', '#tabelas-select', function (e) {
            $j('#edit-table').attr('disabled',false);
            $j('#remove-table').attr('disabled',false);
        });

        $j(document).on('click', '#edit-table', function (e) {
            e.preventDefault();

            var tableSelect = $j('#tabelas-select').val(),
                selectedTable = $j('#'+tableSelect),
                nome = $j(selectedTable).find('[data-type="nome"]').val(),
                campos = $j(selectedTable).find('[data-type="campos"]').val();

            $j('.remodal h1').text('Editar Tabela');
            $j('.remodal').attr('id', tableSelect);
            $j('.remodal').find('#table-name').val(nome);
            $j('.remodal').find('#new-fields').val(campos);

            var modal = $j('[data-remodal-id=modal]').remodal();
            modal.open();
        });

        $j(document).on('click', '#remove-table', function (e) {
            e.preventDefault();
            var tableSelect = $j('#tabelas-select').val();

            $j('#tabelas-select option[value="'+tableSelect+'"]').remove();
            $j('#'+tableSelect).remove();

            if(!$j('#tabelas-select option').length) {
                $j('#edit-table').attr('disabled',true);
                $j('#remove-table').attr('disabled',true);

                $j('#tabelas-select').append('<option selected disabled>Nenhuma tabela encontrada...</option>')
            }
        });

    }

});
