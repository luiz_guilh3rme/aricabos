Handlebars.registerHelper('fields', function(campos, options) {
    //var out = "<ul class=\"review-stars pull-left clearfix\">";
    var out = '';

    for(var c=0; c < campos.length; c++) {
        out += '<div class="form-group sc-col-md-3">';
        out += '<label>' + campos[c]["commonName"] +'</label>';
        out += '<input type="text" class="form-control" name="variacoes['+options.data.root.count+']['+ (c + 1) +']['+campos[c]["slug"]+']"/>';
        out += '</div>';
    }
    return out + "</ul>";
});