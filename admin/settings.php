<?php
// Change Logo
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo_admin.png);
            background-repeat:no-repeat;
            padding-bottom: 30px;
            width: 320px;
            background-size: cover;
            height: 61px;
            background-position: 0 center;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// ==== Add Menus ====
function setup_theme_admin_menus() {
    /* DOC:
   * Parent FileName, Page Title, Menu Title, Capatibility, Menu Slug, Front End Function
   * */
    global $menuzin;
    $menuzin = add_menu_page('Opções do Tema', 'Opções do Tema', 'manage_options', 'theme-options', 'theme_options_render');
    //              Hook,       Page Title,             Menu Title,         Capatibility,     Slug,     Func?
    add_submenu_page('theme-options', 'Informações de Contato', 'Informações', 'manage_options', 'theme-options');

    // Enqueue Scripts
    add_action('admin_enqueue_scripts', 'my_admin_enqueue_scripts');
    function my_admin_enqueue_scripts($hook)    {
        global $menuzin;
        wp_register_style( 'settings_page', get_stylesheet_directory_uri() . '/admin/css/settings_page.css', array(), '1', 'all' );
        wp_register_style( 'remodal', get_stylesheet_directory_uri() . '/admin/css/remodal.css', array(), '1', 'all' );
        wp_register_style( 'remodal-theme', get_stylesheet_directory_uri() . '/admin/css/remodal-default-theme.css', array(), '1', 'all' );
        wp_register_script('handlebars', get_stylesheet_directory_uri() . '/admin/js/handlebars.js');
        wp_register_script('remodal', get_stylesheet_directory_uri() . '/admin/js/remodal.min.js');
        wp_register_script('settings_page', get_stylesheet_directory_uri() . '/admin/js/settings_page.js');

        if ($menuzin == $hook) {
            wp_enqueue_style('settings_page');
            wp_enqueue_style('remodal');
            wp_enqueue_style('remodal-theme');
            wp_enqueue_script('handlebars');
            wp_enqueue_script('remodal');
            wp_enqueue_script('settings_page');
        }
    }
}
add_action("admin_menu", "setup_theme_admin_menus");

// ==== Add Page Plugin ====
function theme_options_render() {
    if( isset( $_GET[ 'tab' ] ) ) $active_tab = $_GET[ 'tab' ];
    else $active_tab = 'tab_one';
    ?>

    <div id="theme-options-wrap" class="widefat <?php if ($active_tab == 'tab_two') echo ' full' ?>"
         xmlns="http://www.w3.org/1999/html">
        <div class="icon32" id="icon-tools"> <br/> </div>
        <h1>Opções do Tema</h1>
        <p> Configure opções globais e aspectos do sistema. </p>

        <h2 class="nav-tab-wrapper">
            <a href="?page=theme-options&tab=tab_one" class="nav-tab <?php echo $active_tab == 'tab_one' ? 'nav-tab-active' : ''; ?>">Tabelas</a>
<!--            <a href="?page=theme-options&tab=tab_two" class="nav-tab --><?php //echo $active_tab == 'tab_two' ? 'nav-tab-active' : ''; ?><!--">Textos</a>-->
        </h2>

        <form method="post" action="options.php"  enctype="multipart/form-data">
            <?php
            if( $active_tab == 'tab_one' ) {
                settings_fields( 'page_1_section_option' );
                do_settings_sections( 'page_1_section_option' );
            } else if( $active_tab == 'tab_two' )  {
                settings_fields( 'page_2_section_option' );
                do_settings_sections( 'page_2_section_option' );
            }
            ?>

            <p class="submit">
                <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>"/>
            </p>
        </form>
    </div>

    <?php
}

/* ----------------------------------------------------------------------------- */
/* Setting Sections And Fields */
/* ----------------------------------------------------------------------------- */
function register_and_build_fields() {

    /* ----------------------------------------------------------------------------- */
    /* Sections Groups  */
    /* ----------------------------------------------------------------------------- */
    add_settings_section(
        'page_1_section', // ID used to identify this section and with which to register
        'Tabelas personalizadas', // Title to be displayed on the administration page
        'page_1_section_callback', // Callback used to render the description of the section
        'page_1_section_option'  // Page on which to add this section of options
    );function page_1_section_callback() {}
    add_settings_section(
        'page_2_section', // ID used to identify this section and with which to register
        'Textos das Etapas', // Title to be displayed on the administration page
        'page_2_section_callback', // Callback used to render the description of the section
        'page_2_section_option'  // Page on which to add this section of options
    );function page_2_section_callback() {}

    /* ----------------------------------------------------------------------------- */
    /* Section Group 1 Items */
    /* ----------------------------------------------------------------------------- */
    add_settings_field(
        'tabelas', // ID
        'Tabelas:', // LABEL
        'tabelas_callback', // CALLBACK FUNCTION
        'page_1_section_option',  // MENU PAGE SLUG
        'page_1_section', // SECTION ID,
        array('class'=> 'block-label')
    );

    /* ----------------------------------------------------------------------------- */
    /* Section Group 2 Items */
    /* ----------------------------------------------------------------------------- */

    /* ----------------------------------------------------------------------------- */
    /* Save Callbacks - Register Section Groups */
    /* ----------------------------------------------------------------------------- */
    register_setting('page_1_section_option', 'page_1_section_option', 'validate_setting');
    register_setting('page_2_section_option', 'page_2_section_option');

}
add_action('admin_init', 'register_and_build_fields');

/* ----------------------------------------------------------------------------- */
/* Field Callbacks */
/* ----------------------------------------------------------------------------- */
function tabelas_callback() {
    $options = get_option('page_1_section_option');

    $tabelas = isset($options['tabelas']) ? array_filter($options['tabelas']) : array(); ?>
    <div class="sc-row" id="tabelas">
        <div class="sc-col-md-6">
            <div class="form-group">
                <?php echo '<select id="tabelas-select" class="form-control">';
                if (count($tabelas) >= 1):
                    $key = 0;
                    foreach ($tabelas as $chave => $tabela){ ?>
                        <option value="tabela_<?php echo $tabela['id']; ?>"><?php echo $tabela['nome']; ?></option>
                        <?php $key++; }
                else:
                    echo '<option disabled selected>Nenhuma tabela encontrada...</option>';
                endif;
                echo '</select>'; ?>
            </div>
        </div>
        <div class="sc-col-md-3">
            <button id="edit-table" class="btn btn-block btn-primary" <?php if (!count($tabelas) >= 1) echo 'disabled'; ?>>Editar</button>
        </div>
        <div class="sc-col-md-3">
            <button id="remove-table" class="btn btn-block btn-danger" <?php if (!count($tabelas) >= 1) echo 'disabled'; ?>>
                <span class="glyphicon glyphicon-remove"></span> Remover</button>

        </div>
    </div>

    <ul id="hidden-tables">
        <?php $key=0; foreach ($tabelas as $chave => $tabela){ ?>
        <li id="tabela_<?php echo $tabela['id']; ?>">
            <!-- Table Name -->
            <input type="hidden" data-type="nome" name="page_1_section_option[tabelas][<?php echo $chave; ?>][nome]" value="<?php echo $tabela['nome']; ?>"/>

            <!-- ID -->
            <input type="hidden" data-type="id" name="page_1_section_option[tabelas][<?php echo $chave; ?>][id]" value="<?php echo $tabela['id']; ?>"/>

            <!-- Campos-->
            <input type="hidden" data-type="campos" name="page_1_section_option[tabelas][<?php echo $chave; ?>][campos]" value="<?php
            for($i=0; $i<count($tabela['campos']); $i++):
                echo $tabela['campos'][$i]['commonName'];
                if(!($i+1 == count($tabela['campos']))) echo ',';
            endfor;
            ?>"/>
        </li>
        <?php $key++; } ?>
    </ul>

    <button data-remodal-target="modal" id="adicionar-tabela" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span>
        Adicionar tabela personalizada
    </button>

    <div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h1 id="modal-title">Criar tabela</h1>
        <p>
            Digite um nome para essa tabela.
        </p>
        <input type="text" id="table-name" class="form-control table_name" placeholder="Defina um nome para a tabela..."/>

        <p>
            Digite os campos que deseja que a tabela possua separando-os por virgulas, EX: Tamanho (kg), Altura (M).
        </p>
        <div class="form-group">
            <input type="text" id="new-fields" placeholder="Nome dos campos separados por vírgula" class="form-control table_name"/>
        </div>

        <div class="clearfix"></div>
        <button data-remodal-action="cancel" class="remodal-cancel">Cancelar</button>
        <button data-remodal-action="confirm" class="remodal-confirm">Salvar</button>
    </div>

    <script id="template" type="text/x-handlebars-template">
        <li id="tabela_{{count}}">
            <input type="hidden" data-type="nome" name="page_1_section_option[tabelas][{{count}}][nome]" value="{{nome}}"/>
            <input type="hidden" data-type="campos" name="page_1_section_option[tabelas][{{count}}][campos]" value="{{campos}}"/>
        </li>
    </script>
<?php
}

function redes_sociais_callback(){
    $options = get_option('page_1_section_option');
    $redes_sociais = $options['redes_sociais']; ?>
    <div class="form-group">
        <label class="block-label" for="facebook">Facebook</label>
        <input class="form-control" placeholder='Facebook URL Ex: redesocial.com.br/suapagina' id="facebook" name='page_1_section_option[redes_sociais][facebook]' type='text' value='<?php echo $redes_sociais['facebook'] ?>' /> <br/>
    </div>

    <div class="form-group">
        <label class="block-label" for="twitter">Twitter</label>
        <input class="form-control" placeholder='Twitter URL Ex: redesocial.com.br/suapagina' id="twitter" name='page_1_section_option[redes_sociais][twitter]' type='text' value='<?php echo $redes_sociais['twitter'] ?>' /> <br/>
    </div>

    <div class="form-group">
        <label class="block-label" for="googleplus">Google Plus</label>
        <input class="form-control" placeholder='Google Plus URL Ex: redesocial.com.br/suapagina' id="googleplus" name='page_1_section_option[redes_sociais][plus]' type='text' value='<?php echo $redes_sociais['plus'] ?>' /> <br/>
    </div>
<?php
}

// ==== Save Functions ====
function validate_setting($plugin_options) {

    if(isset($plugin_options['tabelas'])){

        // Get MasterId
        $masterId = get_option('tdId');

        // If a MasterId is not created
        if(!$masterId){
            $addResult = update_option('tdId', 0);
            $masterId = 0;
        }

        foreach($plugin_options['tabelas'] as $chave => $tabela){

            if(!isset($tabela['id'])){

                $lastId = $masterId + 1;

                // MASTER ID
                update_option( 'tdId', $lastId );

                // Add Id to table
                $plugin_options['tabelas'][$chave]['id'] = $lastId;
            }

            $campos = explode(',', $tabela['campos']);
            $newCampos = array();

            foreach($campos as $chaveCampo => $campo) {
                $campoObject = array('commonName'=>'', 'slug'=>'');

                $sluged =  slugify( $campo );

                $campoObject['commonName']  = $campo;
                $campoObject['slug']        = $sluged;

                array_push($newCampos, $campoObject);
            }

            $plugin_options['tabelas'][$chave]['campos'] = $newCampos;
        }
    }

    return $plugin_options;
}

function slugify($text) {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}
?>