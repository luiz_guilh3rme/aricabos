<?php get_header();

$showFooter = true;

?>

<section id="slider" class="carousel slide hidden-xs" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="carousel-caption">
                <h2>A segurança não é opção!</h2>
                <p>Aricabos trabalha com responsabilidade na elevação e movimentação segura</p>
            </div>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner1-aricabos.jpg" width="100%" alt="Chania">
            <div class="after"></div>
        </div>

        <div class="item">
            <div class="carousel-caption">
                <h2>Qualidade e alto desempenho!</h2>
                <p>Cliente 100% satisfeito e garantimos a rapidez na entrega!</p>
            </div>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner2-aricabos.jpg" width="100%" alt="Chania">
            <div class="after"></div>
        </div>

        <div class="item">
            <div class="carousel-caption">
                <h2>Fornecimento específicos <br>para grandes projetos!</h2>
                <p>Aricabos tem 46 anos de experiência com tecnologia de ponta para seus clientes!</p>
            </div>

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner3-aricabos.jpg" width="100%" alt="Flower">
            <div class="after"></div>
        </div>
    </div>

    <a href="#sobre"><div class="arrow-down"><i class="fa fa-arrow-down"></i></div></a>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>

    <a class="right carousel-control" href="#slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Próximo</span>
    </a>
</section>


<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7">
                <article class="artigo" id="sobre">
                    <h2 class="titulo">Sobre nós</h2>
                    <p class="paragrafo">
                        Desde 1968, a Aricabos se tornou uma das empresas de maior credibilidade no mercado, graças ao seu pensamento zeloso e claro em torno do cliente. Nossas melhorias sempre foram especificadas ao teu gosto. Por essa razão, nós temos uma grande quantidade de materiais prontos para te ajudar em qualquer obra.
                    </p>

                    <a href="<?= site_url('/empresa'); ?>" id="saibaMais" class="btn btn-default">Saiba mais</a>
                </article>
            </div>

            <div class="col-lg-5 col-md-5 hidden-sm">
                <div class="guindaste"></div>
            </div>
        </div>

  <!--       <div class="row">
            <div class="col-sm-12">
                <artcile class="artigo clientes">
                    <h2 class="titulo">Nossos clientes</h2>
                    <div id="clientes-carousel" class="owl-carousel marginT62">
                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-alcoa.jpg" alt="Alcoa" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-gm.jpg" alt="GM" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-nestle.jpg" alt="Nestle" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-odebrecht.jpg" alt="Odebrecht" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-queiroz-galvao.jpg" alt="Queiroz Galvão" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-vale.jpg" alt="Vale" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-votorantim.jpg" alt="Votorantim" />
                        </div>

                        <div class="cliente">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-zf.jpg" alt="ZF" />
                        </div>

                    </div>
                </artcile>
            </div>
        </div> -->

    </div>
</section>



<section id="produtos" class="container">
    <div class="col-lg-12">
        <h2 class="titulo-big"><span>Nossos<br><strong>Produtos</strong></span></h2>
        <p class="center txt-hero">Nossa empresa se orgulha em oferecer para você materiais de alta qualidade, precisos e confiáveis. Nós temos um grande leque de equipamentos adequados para qualquer tipo de obra. Se tranquilize com a gente.</p>
    </div>

    <div class="col-lg-12 marginT62">
        <ul id="lista-produtos" role="tablist">
            <?php
            $blogCatId = get_category_by_slug('blog');
            $args = array(
                'type'                     => 'post',
                'orderby'                  => 'name',
                'parent'                   => 0,
                'order'                    => 'ASC',
                'exclude'                  => $blogCatId->term_id,
                'number'                   => '12',
                'taxonomy'                 => 'category',

            );

            $categories = get_categories( $args );
            $count=0;

            foreach($categories as $key => $category):
                $id         = $category->term_id;
                $link       = get_term_link(intval($id), 'category');
                $options    = get_option('categoria_'.$category->term_id);
                $attchUrl   = wp_get_attachment_image_src( $options['attachment_id'], 'ico-thumb' );
            ?>

                <li role="presentation" class="<?php if($count === 0) echo ' active'; ?>">
                    <a href="#produto<?php echo $key; ?>" data-responsive-link="<?= $link ?>" aria-controls="produto1" role="tab" data-toggle="tab">
                        <div class="blue-icon">
                            <img src="<?php echo $attchUrl[0] ?>"/>
                        </div>

                        <span class="legend-icon"><?= $category->name; ?></span>
                    </a>
                </li>

                <?php $count++; endforeach; ?>
        </ul>
    </div>



    <div id="produtos-content" class="col-lg-12 hidden-xs marginT62">

        <div class="tab-content">

            <?php

            $count=0;

            foreach($categories as $key => $category):

                $options  = get_option('categoria_'.$category->term_id);

                $id = $category->term_id;

                $link = get_term_link(intval($id), 'category');

                $content = apply_filters('the_content', category_description( $category->term_id));

                $featured = wp_get_attachment_image( $options['featured_image_id'], 'full',false, array('class' => 'img-responsive featured'));



                ?>

                <div role="tabpanel" class="tab-pane <?php if($count === 0) echo ' active'; ?>" id="produto<?php echo $key; ?>">

                    <div class="row">

                        <div class="col-sm-4">

                            <?=$featured?>

                        </div>

                        <div class="col-sm-7 thumbnail-height">

                            <h3 class="titulo-yellow"><?php echo $category->name; ?></h3>

                            <p class="marginT30"><?php echo substr($content, 0 , 399); ?>...</p>



                            <a href="<?php echo $link ?>" class="btn btn-primary marginT62">Ir para a categoria</a>



                        </div>

                    </div>



                </div>

                <?php $count++;  endforeach; ?>

        </div>

    </div>



</section>



<section id="aplicacoes" class="container marginT62">

    <div class="col-lg-12">

        <h2 class="titulo-big">

            <span>Possíveis<br>

                <strong>Aplicações</strong>

            </span>

        </h2>

        <p class="center txt-hero">No momento de adquirir um produto, várias empresas acabam fazendo confusão sobre suas utilidades. Pensando nisso, a Aricabos mostra para você em quais funções cada um se adequa, deixando sua obra mais correta.</p>

    </div>



    <?php

    $tags = get_terms('post_tag', array('hide_empty'=> false));



    if ($tags) {

        foreach ($tags as $tag) {

            $attchId    = get_option('tag_'.$tag->term_id);

            $attchUrl   = wp_get_attachment_image_src( $attchId['attachment_id'], 'ico-thumb' );

            $id         = $tag -> term_id;

            $title      = $tag->name;



            if(strlen($title) > 39 ):

                $words = explode(' ', $title);

                $title = $words[0];

            endif;



            ob_start();

            echo $tag->description;

            $old_content = ob_get_clean();

            $new_content = strip_tags($old_content);

            $content_excerpt = substr($new_content, 0 ,90).'...';



            ?>

            <article class="aplicacao col-lg-4 col-md-4">

                <div class="blue-icon"><img src="<?php echo $attchUrl[0] ?>"/></div>

                    <div class="apli-info">

                        <h3><?= $title; ?></h3>

                        <p>

                            <?=$content_excerpt ?>

                        </p>

                    </div>

                <a href="<?= get_tag_link( $id ) ?>" class="btn-inhome">Ver produtos</a>

            </article><?php

        }

    }

    ?>

</section>



<?php get_footer(); ?>

