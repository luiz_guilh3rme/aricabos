<?php
// Template Name: Orcamento
// Necessário criar Pagina Wordpress com o slug /orcamento/

get_header();
$url_orcamento  = get_bloginfo('url').'/orcamento/';
$orc            = $_SESSION['orcamento'];

if(isset($_GET['acao'])) {

    # Prepare DATA
    if(isset($_POST['items'])){
        $items = array();

        foreach($_POST['items'] as $item){
            $has_meters     = get_post_meta($_POST['product_id'], 'has_meters', true);

            if($item['quantidade'] > 0 && $has_meters == 'yes' && $item['metros'] > 0){
                array_push($items, $item);
            }
            else if($item['quantidade'] > 0 && $has_meters != 'yes') {
                array_push($items, $item);
            }
        }

        // GET
        if(isset($_POST['product_name'])) $nome = $_POST['product_name'];
        if(isset($_POST['product_id'])) $id = $_POST['product_id'];
    }

    # Do Action
    switch ($_GET['acao']) {
        case 'adicionar':

            $orcamento = array($nome,$id, $items);
            $gethash = array($nome,$id);

            // Gera hash
            $hash = sha1(implode('', $gethash));

            if(isset($orc[$hash])){
                foreach($items as $key => $item){

                    $exists=false;

                    // Search
                    foreach($orc[$hash]['items'] as $orcKey => $orcItem){
                        if($item['referencia'] == $orcItem['referencia']){
                            $orc[$hash]['items'][$orcKey]['quantidade'] += $item['quantidade'];
                            $orc[$hash]['items'][$orcKey]['metros'] += $item['metros'];
                            $exists = true;
                            break;
                        }
                    }
                    if(!$exists)
                        array_push($orc[$hash]['items'], $item);

                }
                $_SESSION['orcamento'] = $orc;

                header("Location: $url_orcamento");
                exit();
            }
            else {
                if(count($items) != 0){
                    $orc[$hash] = array(
                        'nome'		    => $nome,
                        'id'			=> $id,
                        'items'         => $items
                    );

                    // Reference data
                    $_SESSION['orcamento'] = $orc;
                    header("Location: $url_orcamento");
                    exit();

                }else{
                    header("Location: $url_orcamento");
                }
            }
            break;

        case 'atualizar':
            $hash           = $_GET['hash'];
            $updatedItem    = $_GET['item'];
            $novaQtde       = $_POST['atualizar-quantidade'];
            $novosMetros    = $_POST['atualizar-metros'];

            if (isset($_SESSION['orcamento'][$hash])) {

                if(isset($_GET['item'])){

                    foreach ($orc[$hash]['items'] as $chave => &$item) {

                        if($item['referencia'] == $updatedItem){
                            $item['quantidade'] = $novaQtde;
                            $item['metros']     = $novosMetros;
                        }
                    }
                }

                // Reference Data
                $_SESSION['orcamento'] = $orc;
                header("Location: $url_orcamento");

                exit();
            }
            break;

        case 'deletar':
            $hash = $_GET['hash'];

            if (isset($_SESSION['orcamento'][$hash])) {

                if(isset($_GET['item'])){

                    foreach ($orc[$hash]['items'] as $chave => &$item) {
                        if($item['referencia'] == $_GET['item']){

                            unset($orc[$hash]['items'][$chave]);

                            if(count($orc[$hash]['items']) == 0){
                                unset($orc[$hash]);
                            }
                        }
                    }
                }else{
                    unset($orc[$hash]);
                }

                // Reference Data
                $_SESSION['orcamento'] = $orc;
                header("Location: $url_orcamento");

                exit();

            }
            break;
    }
}
?>
<section id="banner">
    <div class="produto">
        <div class="container">
            <h1 class="titulo-banner"><span>Orçamento</span></h1>
            <h2>SEU CARRINHO DE ORÇAMENTO...</h2>
        </div>
    </div>
</section>
<section id="content">
    <?php if ( function_exists('yoast_breadcrumb') ){
        yoast_breadcrumb('<div class="breadcrumb">','</div>');
    } ?>

    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-lg-12 col-md-12">
                <h2 class="titulo">Item adicionados</h2>
                <p>
                    Para solicitar o orçamento dos produtos, basta selecioná-los em nosso
                    catálogo virtual e clicar em adicionar a cotação.
                    Caso já tenha incluído todos os itens que deseja, preencha os campos abaixo e envie sua solicitação.
                </p>
                <p>
                    Em breve, enviaremos ao seu e-mail todos os detalhes da cotação.
                </p>
                <?php if(count($_SESSION['orcamento']) > 0) : ?>

                    <?php foreach ( $_SESSION['orcamento'] as $hash => $object ) :
                    if(gettype($object) == 'array') :
                        $permalink      = get_permalink($object['id']);
                        $nomeProduto    = get_the_title($object['id']);
                        $has_meters     = get_post_meta($object['id'], 'has_meters', true);?>

                        <div class="thead"><?php echo $nomeProduto; ?></div>
                        <table class="column full tabela-orcamento" width="100%" cellpadding="10" cellspacing="2">
                            <thead>
                                <tr>
                                    <th>Referência</th>
                                    <?php if($has_meters === 'yes'){ ?>
                                        <th>Metros</th>
                                    <?php } ?>
                                    <th>Quantidade</th>
                                    <th>Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($object['items'] as $ky => $item): ?>
                                <tr>
                                    <td data-titulo="Referência" width="50%" class="item-tabela">
                                        <?php echo $item['referencia']; ?>
                                    </td>
                                    <form class="form-inline" action="<?php bloginfo('url'); ?>/orcamento/?acao=atualizar&amp;hash=<?php echo $hash; ?>&amp;item=<?php echo $item['referencia']; ?>" method="post">
                                        <?php if($has_meters === 'yes'){ ?>
                                        <td data-titulo="Metros" class="item-tabela">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input class="form-control" type="number" name="atualizar-metros" min="1" step="any" value="<?php echo $item['metros']; ?>">
                                                    <div class="input-group-addon"> <button type="submit" class="b-atualizar btn btn-sm btn-link">
                                                            <span class="glyphicon glyphicon glyphicon-repeat"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <?php } ?>
                                        <td data-titulo="Quantidade" class="item-tabela">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="atualizar-quantidade" min="1" step="any" value="<?php echo $item['quantidade']; ?>">
                                                    <div class="input-group-addon"> <button type="submit" class="b-atualizar btn btn-sm btn-link">
                                                            <span class="glyphicon glyphicon glyphicon-repeat"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </form>
                                    <td data-titulo="Excluir" class="item-excluir">
                                        <a href="?acao=deletar&amp;hash=<?php echo $hash?>&amp;item=<?php echo $item['referencia']; ?>" class="b-excluir btn btn-link" title="Excluir">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table><?php
                        endif;
                    endforeach; ?>
                    <button
                            title="Finalizar Orçamento"
                            id="finalizar-orcamento"
                            class="b-finaliza-orcamento btn btn-primary pull-right btn-md"
                            data-toggle="modal" data-target="#finish-orcamento">
                        Finalizar Orçamento
                    </button>
                    <a href="<?php bloginfo('url'); ?>/" title="Adicionar Mais Produtos" class="b-add-orcamento btn btn-secondary pull-right btn-md">Adicionar Mais Produtos</a>
                <?php else: ?>
                    <p>Selecione pelo menos um produto!</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="finish-orcamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Finalizar orçamento</h4>
            </div>
            <form id="orcamento" action="<?php echo get_stylesheet_directory_uri(); ?>/inc/envia.php" method="POST">
                <input type="hidden" name="tipo" value="orcamento" />

                <div class="modal-body">
                    <div class="form-group">
                        <label for="nome" class="sr-only">Nome*:</label>
                        <input type="text" placeholder="Nome" name="nome" id="nome" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="email" class="sr-only">Email*:</label>
                        <input type="email" placeholder="Email" name="email" id="email" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="telefone" class="sr-only">Telefone:</label>
                        <input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="form-control celular-input" >
                    </div>
                    <div class="form-group">
                        <label for="cep" class="sr-only">CEP:</label>
                        <input type="tel" placeholder="CEP" data-mask="00000-000" name="cep" id="cep" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="mensagem" class="sr-only">Mensagem:</label>
                        <textarea name="mensagem" placeholder="Mensagem" id="mensagem" class="form-control"></textarea>
                    </div>
                    <span class="aviso-form">*Dados Obrigatórios</span>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar orçamento</button>
                </div>

            </form>
        </div>
    </div>
</div>
<?php
    $tag = get_post_custom_values('tag_google_adwords');
    foreach ( $tag as $tag => $valor ) {
        echo "$valor ";
    }
?>
<?php get_footer(); ?>
