<?php

// CONFIG ===================================/
require_once('admin/settings.php');
require_once('admin/tinyoncat.php');
require_once('inc/wp_bootstrap_navwalker.php');

// Navwalker Config

if ( ! function_exists( 'wpt_setup' ) ){

    function wpt_setup() {
        register_nav_menu( 'primary', __( 'Menu topo', 'wptuts' ) );

        // Footer navs
        register_nav_menu( 'footer-1', __( 'Menu Rodapé 1', 'wptuts' ) );
        register_nav_menu( 'footer-2', __( 'Menu Rodapé 2', 'wptuts' ) );
        register_nav_menu( 'footer-3', __( 'Menu Rodapé 3', 'wptuts' ) );
        register_nav_menu( 'footer-4', __( 'Menu Rodapé 4', 'wptuts' ) );
    }
}

add_action( 'after_setup_theme', 'wpt_setup' );



// Add Post Sizes

if ( function_exists( 'add_image_size' ) ) {
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'list-thumb', 475, 475, true );
    add_image_size( 'galeria-produto', 475, 300, true );
    add_image_size( 'post-thumb', 566, 240, true );
    add_image_size( 'thumb-galeria-produto', 95, 60, true );
    add_image_size( 'service-thumb', 265, 265, true );
    add_image_size( 'ico-thumb', 45, 45, true );
}



// METABOXES ================================/

add_action( 'add_meta_boxes', 'add_metabox' );
function add_metabox() {


    // ADD GALERY
    $types = array( 'post', 'page' );
    foreach( $types as $type ) {
        add_meta_box('galeria_metabox', 'Galeria', 'galeria_metabox', $type);
    }



    add_meta_box(

        'tabelas_metabox',

        'Tabela de especificção',

        'tabelas_metabox',

        'post'

    );



    add_meta_box(

        'has_meters',

        'É mensurável em metros ?',

        'has_meters',

        'post',

        'side',

        'high'

    );

}



// CUSTOM FIELDS ===========================/

function galeria_metabox( $post ) {

    $galeria =  unserialize(get_post_meta($post->ID, 'galeria', true ));

    $attch_id = !empty($galeria) ? $galeria : array(); ?>



    <style>

        .wrap-galery {

            padding: 0 15px;

        }

        .galeria{width: 100%;            }

        .galeria:after,.galeria:before {

            content: '';

            display: table;

        }

        .galeria:after{clear: both;}



        .galeria li{

            width: 25%;

            float: left;

            display: block;

            margin-bottom: 20px;

        }

        .galeria li .content {

            padding: 0 5px;

        }



        .galeria li:nth-child(4n+1){

            margin-left: 0;

        }



        .galeria .output-imagem{

            width: 100%;

            height: 250px;

            background-size: cover !important;

            display: block;

            margin-bottom: 10px;

            border: 2px solid #CECECE;

        }



        .galeria .b-upload{

            width: 100%;

            display: inline-block;

            margin-bottom: 5px;

        }

        .galeria .b-remover{

            width: 100%;

            display: inline-block;

            background-color: #ff5b5b;

            border-color: #c91a1a;

            color: white;

            text-align: center;

        }

        .galeria .b-remover:hover{

            background-color: #dc3c3c;

            color: white;

        }

    </style>



    <ul id="galeria_pagina_list" class="galeria">



        <?php foreach ($attch_id as $key => $value) : ?>

            <li>

                <div class="content">

                    <input type="hidden" name="galeria[]" class="imagem-id" value="<?php echo $value; ?>"/>

                    <span class="output-imagem" style="background: url('<?php $img = wp_get_attachment_image_src($value, 'galeria-produto'); echo $img[0]; ?>') no-repeat center center; background-size: cover;"></span>

                    <input type="button" class="button button-primary b-upload" value="Troca Imagem" />

                    <input type="button" class="button b-remover" value="Remover" />

                </div>

            </li>

        <?php endforeach; ?>

    </ul>



    <a href="javascript:;" id="imageAdder">Adicionar nova imagem</a>



    <script type="text/x-handlebars-template" id="fastTemplate">

        <li>

            <div class="content">

                <input type="hidden" name="galeria[]" class="imagem-id" value="{{imageId}}"/>

                <span class="output-imagem" style="{{style}}"></span>

                <input type="button" class="button button-primary b-upload" value="Troca Imagem" />

                <input type="button" class="button b-remover" value="Remover" />

            </div>

        </li>

    </script>

<?php

}

function tabelas_metabox( $post ) {

    $options = get_option('page_1_section_option');

    $tabelas = isset($options['tabelas']) ? $options['tabelas'] : array();

    $tableExist = false;

    $variacoes   = get_post_meta($post->ID, 'variacoes', true);

    $selectedTable  = get_post_meta($post->ID, 'tabela_selecionada', true);



    ?>



    <!-- Table Selector-->

    <div class="form-group">

        <?php echo '<select id="tabelas-select" name="tabela_selecionada" class="form-control">';

        if (count($tabelas) >= 1) {

            if (!$tableExist) {

                echo '<option></option>';

            }



            $key = 0;

            foreach ($tabelas as $chave => $tabela) { ?>

                <option

                    data-campos="<?= str_replace("\"", "'",json_encode($tabela['campos']) );?>"

                    value="tabela_<?php echo $tabela['id'];?>"

                    <?php if (('tabela_' . $tabela['id']) === $selectedTable): echo ' selected';

                        $tableExist = true; endif; ?>>

                    <?php echo $tabela['nome']; ?>

                </option>

                <?php $key++;

            }



        }else {

            echo '<option disabled selected>Nenhuma tabela encontrada...</option>';

        }

        echo '</select>'; ?>



    </div>



    <!-- Variações -->

    <ul id="variacoes" class="variacoes">

        <?php



        if(!empty($variacoes) && $tableExist) {

            $count = 0;

            foreach ($variacoes as $key => $variacao) : ?>



                <!-- Variação -->

                <li id="var_<?php echo $count; ?>" class="variacao sc-row clearfix">

                    <div class="sc-col-lg-12">

                        <button type="button" class="close remove" aria-label="Close"><span class="text">Remover</span> <span aria-hidden="true">&times;</span></button>

                    </div>

                    <input type="hidden" name="variacao_<?php echo $count; ?>">

                    <div class="sc-col-lg-12 sc-col-md-12">

                        <div class="sc-row">

                        <?php



                        // Search table by slug

                        foreach($tabelas as $chaveTable => $tabela):



                            // When find the table

                            if(('tabela_'.$tabela['id']) === $selectedTable) { ?>



                                <div class="form-group sc-col-md-3">

                                <label>Referência</label>

                                <input type="text" value="<?php echo $variacao[0]['referencia'] ?>" class="form-control"

                                       name="variacoes[<?php echo $count ?>][0][referencia]"/>

                                </div><?php



                                // Repeat campos

                                foreach ($tabela['campos'] as $chave => $campo) { ?>

                                    <div class="form-group sc-col-md-3">

                                    <label><?php echo $campo['commonName']; ?></label>

                                    <input type="text" value="<?php echo $variacao[$chave + 1][$campo['slug']]; ?>"

                                           class="form-control"

                                           name="variacoes[<?php echo $count ?>][<?= $chave + 1 ?>][<?php echo $campo['slug'] ?>]"/>

                                    </div><?php

                                }

                            }

                        endforeach; ?>



                        </div>

                    </div>

                </li>

                <!-- /Variacao -->

            <?php $count++; endforeach;



            echo '<script> var varialCount = '.  $count .';</script>';

        }

        ?>

    </ul>

    <a href="" class="<?php if(empty($tabelas)) echo ' no-exists'; ?>" id="adicionar-variacao"> Adicionar Linha</a>



    <script id="template" type="text/x-handlebars-template">

        <li id="var_{{count}}" class="variacao sc-row clearfix">

            <div class="sc-col-lg-12">

                <button type="button" class="close remove" aria-label="Close"><span class="text">Remover</span> <span aria-hidden="true">&times;</span></button>

            </div>

            <input type="hidden" name="variacao_{{count}}">



            <div class="sc-col-lg-12 sc-col-md-12">

                <div class="sc-row">

                    <div class="form-group sc-col-md-3">

                        <label>Referência</label>

                        <input type="text" value="" class="form-control" name="variacoes[{{count}}][0][referencia]"/>

                    </div>

                    {{#fields campos}}{{/fields}}

                </div>

            </div>



        </li>

    </script>



    <script>

        $j('#tabelas-select').select2({

            placeholder: "Selecione uma tabela..."

        });



    </script>

<?php

}



function set_father($term) {

    $termid = $term->term_id;

    $blogCat = get_category_by_slug('blog');

    if(cat_is_ancestor_of($blogCat->term_id,  $term ) || $termid === $blogCat->term_id) return;



    //var_dump($term);

    $option = get_option('is_father_'.$termid);



    ?>

    <table class="form-table">

        <tr class="form-field form-required term-name-wrap">

            <th scope="row">

                <label for="is_father">É uma categoria pai?</label>

            </th>

            <td>

                <select name="is_father" id="is_father" class="form-control">

                    <?php

                    if(!empty($option)){

                        if($option == 'yes'){

                            echo '<option selected value="yes">Sim</option>';

                            echo '<option value="no">Não</option>';

                        }else{

                            echo '<option value="yes">Sim</option>';

                            echo '<option selected value="no">Não</option>';

                        }

                    }else{

                        echo '<option value selected disabled>Selecione uma opção...</option>';

                        echo '<option value="yes">Sim</option>';

                        echo '<option value="no">Não</option>';

                    }



                    ?>

                </select>

                <p class="description">É pai de outras categorias ?</p>

            </td>

        </tr>

    </table><?php

}

function has_meters($post) {

    $postid = $post->ID;

    $option = get_post_meta($postid, 'has_meters', true ); ?>



    <table class="form-table">

    <tr class="form-field form-required term-name-wrap">

        <td>

            <select name="has_meters" id="has_meters" class="form-control">

                <?php

                if(!empty($option)){

                    if($option == 'yes'){

                        echo '<option selected value="yes">Sim</option>';

                        echo '<option value="no">Não</option>';

                    }else{

                        echo '<option value="yes">Sim</option>';

                        echo '<option selected value="no">Não</option>';

                    }

                }else{

                    echo '<option value="yes">Sim</option>';

                    echo '<option selected value="no">Não</option>';

                }



                ?>

            </select>

            <p class="description">Selecione sim ou não, para ativar orçamentos por metros nesse produto!</p>

        </td>

    </tr>

    </table><?php

}

function category_upload( $term ){

    $term_id  = $term->term_id;

    $blogCat = get_category_by_slug('blog');



    if(cat_is_ancestor_of($blogCat->term_id,  $term ) || $term_id === $blogCat->term_id) return;

    if($term->taxonomy == 'post_tag')

        $option = get_option("tag_$term_id");

    else

        $option = get_option("categoria_$term_id");



    $attachment_id = !empty($option['attachment_id']) ? $option['attachment_id'] : '';

    $featured_image_id = !empty($option['featured_image_id']) ? $option['featured_image_id'] : '';

    ?>



    <style>

        .output-imagem {

            height: 40px;

            width: 40px;

            overflow: hidden;

            margin-bottom: 5px;

        }



        .output-imagem img {

            width:40px;

            display: block;

            height: auto;

        }

    </style>

    <table class="form-table" id="galeria_categoria">

        <tr class="form-field">

            <th scope="row" valign="top">

                <label>Imagens da Categoria:</label>

            </th>

            <td>

                <ul class="galeria">

                    <label>Icone: </label>

                    <li style="width: 306px;">

                        <input type="hidden" name="option[attachment_id]" class="imagem-id" value="<?php echo esc_attr($attachment_id); ?>" />



                        <div class="output-imagem icone" style="

                            <?= $attachment_id != '' ? 'background-image: url('. wp_get_attachment_image_src( $attachment_id, 'full' )[0] . ');' :

                            'background-image: url( http://placehold.it/290x274/D7D7D7/8F8F8F&amp;text=Selecione );'; ?>"></div>



                        <input type="button" class="button b-upload" value="Upload Imagem" />

                        <a class="button b-remover-category" href="#">Remover</a>

                    </li>



                    <label>Imagem Destacada: </label>

                    <li style="width: 306px;">

                        <input type="hidden" name="option[featured_image_id]" class="imagem-id" value="<?php echo esc_attr($featured_image_id); ?>" />



                        <div class="output-imagem featured" style="

                        <?= $featured_image_id != '' ? 'background-image: url('. wp_get_attachment_image_src( $featured_image_id, 'full' )[0] . ');' :

                            'background-image: url( http://placehold.it/290x274/D7D7D7/8F8F8F&amp;text=Selecione );'; ?>"></div>



                        <input type="button" class="button b-upload" value="Upload Imagem" />

                        <a class="button b-remover-category" href="#">Remover</a>

                    </li>

                </ul>

            </td>

        </tr>

    </table>



    <style type="text/css">

        .b-upload,

        .b-remover-category{

            width: 100%;

        }

        .b-remover-category {

            background-color: #ff5b5b;

            border-color: #c91a1a;

            color: white;

            margin-top: 5px;

            text-align: center;

        }



        .output-imagem {

            background-size:cover;

            background-repeat: no-repeat;

            background-position: center center;

        }



        .output-imagem.featured {

            width: 150px;

            height: 150px;

            display: block;

        }



        .output-imagem.icone {

            width: 40px;

            height:40px;

            display: block;

        }

    </style>

<?php

}



// Change Wrap of Breadcrumb

function filter_wpseo_breadcrumb_output ($output) {



    $html = '<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">';



    // New DOMObject

    $dom = new DOMDocument('1.0', 'UTF-8');



    /// Load HTML

    $dom->loadHTML(mb_convert_encoding($output, 'HTML-ENTITIES', 'UTF-8'));



    // Disable Errors

    libxml_use_internal_errors(true);



    $childCount = 0;



    // Get list of <a> childs

    if($dom->getElementsByTagName('a')->length) {



        foreach($dom->getElementsByTagName('a') as $a){



            $html .= "

            <li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">

                <a itemprop=\"item\" href='{$a->getAttribute('href')}'>

                    <span itemprop=\"name\">

                        {$a->nodeValue}

                    </span>

                    <meta itemprop=\"position\" content=\"{$childCount}\" />

                </a>

            </li>";



            $childCount++;

        }



    }



    // Instance finder.

    $finder = new DomXPath($dom);

    $classname  = 'breadcrumb_last';



    // Search by breadcrumb_last <span>

    if($span = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]")->item(0)) {



        $html .= "

            <li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">

                <strong itemprop=\"item\">

                    <span itemprop=\"name\">

                        {$span->nodeValue}

                    </span>

                    <meta itemprop=\"position\" content=\"{$childCount}\" />

                </strong>

            </li>";



    }



    // Clear Errors

    libxml_clear_errors();



    $html .= '</ol>';



    return $html;



}



// add the filter 

add_filter('wpseo_breadcrumb_output', 'filter_wpseo_breadcrumb_output', 10, 1);



// Actions

add_action('category_edit_form_fields', 'set_father', 10, 2);

add_action('category_edit_form_fields', 'category_upload', 10, 2);



// Tags

add_action('post_tag_edit_form_fields', 'category_upload', 10, 2); // For Edit

add_action('post_tag_add_form_fields', 'category_upload', 10, 2); // For New

add_action('edited_post_tag', 'save_tag');



function save_tag() {



    if(isset($_POST['option'])){

        $id = 'tag_'.$_POST['tag_ID'];

        update_option($id, $_POST['option']);



    }

}



// FUNCTIONS ===============================/

// ==== Pagination Script ====

function custom_pagination($numpages = '', $pagerange = '', $paged, $customClass) {

    global $wp;



    $current_url = home_url(add_query_arg(array(),$wp->request));



    if (empty($pagerange)) {

        $pagerange = 2;

    }



    if (empty($paged)) {

        $paged = 1;

    }

    if ($numpages == '') {

        global $wp_query;

        $numpages = $wp_query->max_num_pages;

        if(!$numpages) {

            $numpages = 1;

        }

    }



    #Preg replace

    #echo preg_replace('/page\/[0-9]/', '/', site_url()) . '%_%';

    $pagination_args = array(

        'base'            => $current_url.'/%_%',

        'format'          => '?page=%#%',

        'total'           => $numpages,

        'current'         => $paged,

        'show_all'        => false,

        'end_size'        => 1,

        'mid_size'        => $pagerange,

        'prev_next'       => true,

        'prev_text'       => '<i class="fa fa-chevron-left"></i>',

        'next_text'       => '<i class="fa fa-chevron-right"></i>',

        'type'            => 'plain',

        'add_args'        => false,

        'add_fragment'    => ''

    );



    $paginate_links = paginate_links($pagination_args);



    if ($paginate_links) {

        echo "<nav class='paginacao clearfix ".$customClass."'>";

        echo $paginate_links;

        echo "</nav>";

    }



}



add_action('template_redirect', 'inherit_cat_template');

function inherit_cat_template() {



    if (is_category()) {



        $catid = get_query_var('cat');

        $category = get_category($catid);

        $catslug = $category->slug;



        if ( file_exists(TEMPLATEPATH . '/category-' . $catid . '.php') ) {

            include( TEMPLATEPATH . '/category-' . $catid . '.php');

            exit;

        } else if (file_exists(TEMPLATEPATH . '/category-' . $catslug . '.php')){

            include( TEMPLATEPATH . '/category-' . $catslug . '.php');

            exit;

        }



        $cat = get_category($catid);

        $parent = $cat->category_parent;



        while ($parent){

            $cat = &get_category($parent);

            if ( file_exists(TEMPLATEPATH . '/category-' . $cat->slug . '.php') ) {

                include (TEMPLATEPATH . '/category-' . $cat->slug . '.php');

                exit;

            }

            $parent = $cat->category_parent;

        }

    }

}



add_action('admin_enqueue_scripts', 'chrome_fix');

function chrome_fix() {

    if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Chrome' ) !== false )

        wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );

}



// SAVE CUSTOM FIELDS POST CALLBACK ========/

add_action( 'save_post', 'post_save' );

function post_save( $post_id )    {



    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)

        return;



    if (!isset($_POST['post_type'])) return;



    if ('page' == $_POST['post_type']) {

        if (!current_user_can('edit_page', $post_id))

            return;

    }

    else {

        if (!current_user_can('edit_post', $post_id))

            return;

    }



    // Galeria

    if (isset($_POST['galeria'])) : update_post_meta($post_id, 'galeria', serialize($_POST['galeria'])); endif;



    // Variações

    if (isset($_POST['variacoes'])) : update_post_meta($post_id, 'variacoes', $_POST['variacoes']);

        else: update_post_meta($post_id, 'variacoes', array());

    endif;



    // Tabela selecionada

    if (isset($_POST['tabela_selecionada'])) :

        update_post_meta($post_id, 'tabela_selecionada', $_POST['tabela_selecionada']);

    endif;



     // Possui Metros

    if (isset($_POST['has_meters'])) :

        update_post_meta($post_id, 'has_meters', $_POST['has_meters']);

    endif;

}



add_action('edited_category', 'salva_options_categoria', 10, 2);

function salva_options_categoria($term_id) {

    if (isset($_POST['option'])) {

        $option   = get_option("categoria_$term_id");

        $cat_keys = array_keys($_POST['option']);



        foreach ($cat_keys as $key) {

            if (isset($_POST['option'][$key]))

                $option[$key] = $_POST['option'][$key];

        }



        update_option("categoria_$term_id", $option);

    }



    if(isset($_POST['is_father'])) {

        $is_father = $_POST['is_father'];



        update_option('is_father_'.$term_id, $is_father);

    }

}



// RE-LABEL ================================/

add_action( 'init', 'wpa4182_init');

function wpa4182_init() {

    global $wp_taxonomies;



    // The list of labels we can modify comes from

    //  http://codex.wordpress.org/Function_Reference/register_taxonomy

    //  http://core.trac.wordpress.org/browser/branches/3.0/wp-includes/taxonomy.php#L350

    $wp_taxonomies['post_tag']->labels = (object)array(

        'name' => 'Aplicações',

        'menu_name' => 'Aplicações',

        'singular_name' => 'Aplicação',

        'search_items' => 'Procurar Aplicações',

        'popular_items' => 'Aplicações populares',

        'all_items' => 'Todas aplicações',

        'parent_item' => null, // Tags aren't hierarchical

        'parent_item_colon' => null,

        'edit_item' => 'Editar',

        'update_item' => 'Atualizar',

        'add_new_item' => 'Adicionar',

        'new_item_name' => 'Nova Aplicação',

        'separate_items_with_commas' => 'Separar aplicações com virgulas',

        'add_or_remove_items' => 'Adicionar ou remover',

        'choose_from_most_used' => 'Selecionar aplicações mais utilizadas',

    );

    $wp_taxonomies['post_tag']->label = 'Aplicações';

}



function change_post_menu_label() {

    global $menu;

    global $submenu;

    $menu[5][0] = 'Produtos';

    $submenu['edit.php'][5][0] = 'Produtos';

    $submenu['edit.php'][10][0] = 'Adicionar Produto';

    echo '';

}

function change_post_object_label() {

    global $wp_post_types;

    $labels = &$wp_post_types['post']->labels;

    $labels->name = 'Produtos';

    $labels->singular_name = 'Produto';

    $labels->add_new = 'Adicionar';

    $labels->add_new_item = 'Adicionar';

    $labels->edit_item = 'Editar';

    $labels->new_item = 'Adicionar';

    $labels->view_item = 'Ver artigo';

    $labels->search_items = 'Procurar produtos';

    $labels->not_found = 'Nenhum produto encontrado';

    $labels->not_found_in_trash = 'Nenhum artigo encontrado na lixeira';



    $wp_post_types['post']->menu_icon = 'dashicons-cart';

}

function add_menu_icons_styles(){ ?>



    <style>

        #adminmenu .menu-icon-post div.wp-menu-image:before {

            content: '\f174';

        }

    </style>



<?php

}

add_action( 'admin_head', 'add_menu_icons_styles' );

add_action( 'init', 'change_post_object_label' );

add_action( 'admin_menu', 'change_post_menu_label' );



// POST TYPES ==============================/

function blog_post_type() {



    $labels = array(

        'name'                => _x( 'Blog', 'Post Type General Name', 'text_domain' ),

        'singular_name'       => _x( 'Blog', 'Post Type Singular Name', 'text_domain' ),

        'menu_name'           => __( 'Blog', 'text_domain' ),

        'name_admin_bar'      => __( 'Blog', 'text_domain' ),

        'parent_item_colon'   => __( 'Item acima', 'text_domain' ),

        'all_items'           => __( 'Todos Itens', 'text_domain' ),

        'add_new_item'        => __( 'Adicionar', 'text_domain' ),

        'add_new'             => __( 'Adicionar', 'text_domain' ),

        'new_item'            => __( 'Novo', 'text_domain' ),

        'edit_item'           => __( 'Editar', 'text_domain' ),

        'update_item'         => __( 'Atualizar', 'text_domain' ),

        'view_item'           => __( 'Ver', 'text_domain' ),

        'search_items'        => __( 'Procurar', 'text_domain' ),

        'not_found'           => __( 'Não encontrado', 'text_domain' ),

        'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'text_domain' ),

    );

    $args = array(

        'label'               => __( 'Blog', 'text_domain' ),

        'description'         => __( 'Postagens do blog', 'text_domain' ),

        'labels'              => $labels,

        'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),

        'taxonomies'          => array( 'category' ),

        'hierarchical'        => true,

        'public'              => true,

        'show_ui'             => true,

        'show_in_menu'        => true,

        'menu_position'       => 5,

        'menu_icon'           => 'dashicons-welcome-write-blog',

        'show_in_admin_bar'   => true,

        'show_in_nav_menus'   => true,

        'can_export'          => true,

        'has_archive'         => true,

        'exclude_from_search' => false,

        'publicly_queryable'  => true,

        'capability_type'     => 'post',

    );

    register_post_type( 'blog', $args );



}

add_action( 'init', 'blog_post_type', 0 );



// MEDIA SCRIPTS ENQUEUE ===================/

function uploader_script() {

    echo '<script src="'. get_bloginfo('template_directory') .'/admin/js/scripts.js"></script>';

    echo '<script src="'. get_bloginfo('template_directory') .'/admin/js/select2.full.min.js"></script>';

    echo '<link rel="stylesheet" href="'. get_bloginfo('template_directory') .'/admin/css/select2.min.css'.'"/>';

}

add_action('admin_head', 'uploader_script');



function add_admin_scripts() {

    wp_register_script('handlebars', get_stylesheet_directory_uri() . '/admin/js/handlebars.js');

    wp_register_script('handlebarsHelpers', get_stylesheet_directory_uri() . '/admin/js/Helpers.js');



    wp_enqueue_script('handlebars');

    wp_enqueue_script('handlebarsHelpers');

    wp_enqueue_script('media-upload');

    wp_enqueue_script('thickbox');

    wp_enqueue_media();

}

function add_admin_styles() {

    wp_register_style( 'settings_page', get_stylesheet_directory_uri() . '/admin/css/settings_page.css', array(), '1', 'all' );

    wp_enqueue_style('settings_page');



    wp_enqueue_style('thickbox');

}

add_action('admin_print_scripts', 'add_admin_scripts');

add_action('admin_print_styles', 'add_admin_styles');



// WORDPRESS PAGINATION =====================/

function wpbeginner_numeric_posts_nav() {



    if( is_singular() )

        return;



    global $wp_query;



    /** Stop execution if there's only 1 page */

    if( $wp_query->max_num_pages <= 1 )

        return;



    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

    $max   = intval( $wp_query->max_num_pages );



    /**	Add current page to the array */

    if ( $paged >= 1 )

        $links[] = $paged;



    /**	Add the pages around the current page to the array */

    if ( $paged >= 3 ) {

        $links[] = $paged - 1;

        $links[] = $paged - 2;

    }



    if ( ( $paged + 2 ) <= $max ) {

        $links[] = $paged + 2;

        $links[] = $paged + 1;

    }



    echo '<div class="navigation"><ul>' . "\n";



    /**	Previous Post Link */

    if ( get_previous_posts_link() )

        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );



    /**	Link to first page, plus ellipses if necessary */

    if ( ! in_array( 1, $links ) ) {

        $class = 1 == $paged ? ' class="active"' : '';



        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );



        if ( ! in_array( 2, $links ) )

            echo '<li>…</li>';

    }



    /**	Link to current page, plus 2 pages in either direction if necessary */

    sort( $links );

    foreach ( (array) $links as $link ) {

        $class = $paged == $link ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );

    }



    /**	Link to last page, plus ellipses if necessary */

    if ( ! in_array( $max, $links ) ) {

        if ( ! in_array( $max - 1, $links ) )

            echo '<li>…</li>' . "\n";



        $class = $paged == $max ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );

    }



    /**	Next Post Link */

    if ( get_next_posts_link() )

        printf( '<li>%s</li>' . "\n", get_next_posts_link() );



    echo '</ul></div>' . "\n";



}



add_action( 'template_redirect', 'my_template_redirect' );

function my_template_redirect() {



    global $wp;

    global $wp_query;



    if (isset($wp_query->query['name']) && $wp_query->query['name'] == 'aplicacoes'){

        add_filter( 'template_include', function() {

            return get_template_directory() . '/taxonomy-post_tag.php';

        });

    }

}



function preVar($var){

    echo "<pre>";

    print_r($var);

    echo "</pre>";

}

?>