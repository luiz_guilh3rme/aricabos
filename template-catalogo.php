<?php

// Template Name: Download Catalogo



get_header();

$options = get_option('page_1_section_option');



if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner"><span>Baixando Catálogo!</span></h1>

                <h2 class="text-uppercase">Confira as novidades da Aricabos!</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-md-6">

                    <article class="artigo" id="sobre">

                        <p>Obrigado! O Link de download foi enviado para o seu e-mail.</p>

                        <a href="<?php echo site_url(); ?>">Voltar para home</a>

                    </article>

                </div>

            </div>



        </div>

    </section>

    <!-- <iframe src="<?= get_stylesheet_directory_uri(); ?>/inc/download.php?download_file=catalogo_aricabos.pdf"  style="display:none;"></iframe> -->

<?php endwhile; endif; ?>

<?php

    $tag = get_post_custom_values('tag_google_adwords');

    foreach ( $tag as $tag => $valor ) {

        echo "$valor ";

    }

?>

<?php get_footer(); ?>