<?php get_header();

global $query_string;



$query_args = explode("&", $query_string);

$search_query = array();



foreach($query_args as $key => $string) {

    $query_split = explode("=", $string);

    $search_query[$query_split[0]] = urldecode($query_split[1]);

}



$paged  = ( get_query_var('page') ) ? get_query_var('page') : 1;



$search_query['posts_per_page'] = 10;

//$search_query['orderby'] = 'title';

//$search_query['order'] = 'ASC';

$search_query['paged'] = $paged;

$search = new WP_Query($search_query);

?>



    <section class="search-page" id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner <?php if(strlen(get_search_query()) > 39) echo ' small-font'; ?>">

                    <small>Resultado para o termo</small>

                    <span><?php echo get_search_query(); ?></span>

                </h1>

                <h2>Encontre mais produtos com a busca Aricabos.</h2>

            </div>

        </div>

    </section>



    <?php

    if(isset($_GET['cat']) && $_GET['cat'] < 0 ){

        if ( $search->have_posts() ) { ?>

            <section id="content">

                <div class="container">

                    <div class="row">

                        <?php while ($search->have_posts()) : $search->the_post();

                            ob_start();

                            the_content();

                            $old_content = ob_get_clean();

                            $new_content = strip_tags($old_content);

                            $content_excerpt = substr($new_content, 0 ,90).'...';

                            $cats = array();



                            // Populate Array of categories

                            foreach(wp_get_post_categories( $post->ID ) as $c){

                                $cat = get_category( $c );

                                $cats[] = array( 'id' => $cat->term_id, 'name' => $cat->name, 'slug' => $cat->slug );

                            }

                        ?>



                            <article class="aplicacao col-sm-12 col-xs-12 col-lg-4 col-md-4">

                                <div class="aplicacao-header">

                                    <?php

                                        if(sizeof($cats)>0)

                                        {

                                            $attchId = get_option('categoria_'.$cats[0]['id']);

                                            $attchUrl = wp_get_attachment_image_src( $attchId['attachment_id'], 'full' );



                                            echo "<div class=\"blue-icon\"><img src=\"{$attchUrl[0]}\"/></div>";

                                        }else{

                                            echo "<div class=\"blue-icon\"><i class=\"fa fa-lightbulb-o\"></i></div>";

                                        }

                                    ?>

                                    <h3 class="aplicacao-title"><?= the_title(); ?></h3>

                                </div>

                                <div class="apli-info">

                                    <p><?php echo $content_excerpt ?></p>

                                    <a href="<?php the_permalink(); ?>" class="btn btn-inverted btn-secondary narrow">Ver Produto</a>

                                </div>

                            </article>

                        <?php endwhile;



                        wpbeginner_numeric_posts_nav();



                        ?>

                    </div>

                </div>

            </section><?php

            } else { ?>

            <p class="aviso-sem-produto">Nenhum produto encontrado!</p>

        <?php }

    }

    else{ ?>

        <section id="blog" class="container marginT62">

            <div class="col-lg-8 col-md-8">

                <?php if ($search->have_posts()) : ?>

                    <div class="post_list">

                        <?php while ($search->have_posts()) : $search->the_post(); ?>

                            <div class="row post">

                                <aside id="social-blog" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">

                                    <div class="post-social">

                                        <div class="date"><?php the_time('d'); ?><br> <?php the_time('M'); ?></div>

                                        <div class="like">LIKE</div>

                                        <div class="icons">

                                            <a href="#"><i class="fa fa-facebook"></i></a>

                                            <a href="#"><i class="fa fa-twitter"></i></a>

                                            <a href="#"><i class="fa fa-pinterest"></i></a>

                                            <a href="#"><i class="fa fa-google-plus"></i></a>

                                            <a href="#"><i class="fa fa-linkedin"></i></a>

                                        </div>

                                    </div>

                                </aside>



                                <div id="blog-content" class="col-lg-10 col-md-10 col-sm-10">

                                    <article>

                                        <?php if(has_post_thumbnail()) the_post_thumbnail('post-thumb', array('class'=>' img-responsive thumb')); ?>

                                        <h4 class="titulo-blog"><?php the_title(); ?></h4>

                                        <div class="extra">

                                            <i class="fa fa-tags"></i> <span>Tag 1, Tag 2</span>

                                            <i class="fa fa-comments marginL20"></i> <span><?php comments_number( 'Nenhum comentário', '1 Comentário', '% Comentários' ); ?></span>

                                        </div>

                                        <p><?php the_excerpt(); ?></p>

                                        <a href="<?php the_permalink(); ?>" class="more">Leia mais <i class="fa fa-long-arrow-right"></i></a>

                                    </article>

                                </div>

                            </div>

                        <?php endwhile; ?>

                    </div>

                    <?php

                    if (function_exists(custom_pagination)) :

                        custom_pagination($search->max_num_pages,"",$paged, 'col-lg-offset-2');

                    endif;

                else:

                    echo _e('Desculpe, não encontramos nenhum post.');

                endif; ?>

            </div>



            <aside id="blog-sidebar" class="col-lg-4 col-md-4">

                <?php require 'inc/sidebar-blog.php'; ?>

            </aside>

        </section><?php

    } ?>



<?php get_footer(); ?>