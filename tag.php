<?php get_header();

global $wp_query;
$tag = $wp_query->queried_object;

//echo $tag->term_id;

$attchId    = get_option('tag_'.$tag->term_id);
$attchUrl   = wp_get_attachment_image_src( $attchId['attachment_id'], 'full' ); ?>

    <section id="banner">
        <div class="produto">
            <div class="container">
                <h1 class="titulo-banner <?php if(strlen(single_tag_title(false, false)) > 39) echo ' small-font'; ?>">
                    <small>Aplicação</small>
                    <span><?php single_tag_title(); ?></span>
                </h1>
                <h2 class="text-uppercase">Produtos de alta qualidade e com o melhor desempenho em vários segmentos</h2>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="col-lg-6 col-md-6">
                <article class="artigo" id="sobre">
                    <p class="paragrafo"><?php echo category_description(); ?></p>
                </article>
            </div>

            <?php require 'inc/form-catalogo.php'; ?>

            <div class="col-lg-12 col-md-12">
                <div class="row aplicacoes-page">
                    <?php
                    $currentCatId = get_cat_id( single_cat_title("",false));

                    global $paged;
                    $curpage = $paged ? $paged : 1;
                    $args = array(
                        'orderby' => 'title',
                        'order'      => 'ASC',
                        'posts_per_page' => -1,
                        'paged' => $paged,

                        'post_type' => 'post',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'post_tag',
                                'field'    => 'id',
                                'terms'    =>  $tag->term_id,
                            ),
                        )
                    );
                    $query  = new WP_Query( $args );

                    // create a new instance of WP_Query;
                    if ( $query->have_posts() ) {
                        while ($query->have_posts()) : $query->the_post();
                            ob_start();
                            the_content();
                            $old_content = ob_get_clean();
                            $new_content = strip_tags($old_content);
                            $content_excerpt = substr($new_content, 0 ,130).'...';

                            $get = unserialize( get_post_meta($post->ID, 'galeria', true ) );
                            $get = wp_get_attachment_image_src($get[0], 'list-thumb');

                            $image = !empty($get) ? $get[0] : $attchUrl[0];
                            ?>

                            <article class="aplicacao col-lg-6 col-md-6">
                                <div class="thumbnail-container">
                                    <img src="<?= $image ?>" class="aplicacao-thumbnail"/>
                                </div>
                                <div class="aplicacao-header">
                                    <h3 class="aplicacao-title"><?php the_title(); ?></h3>
                                </div>
                                <div class="apli-info">
                                    <p><?= $content_excerpt ?></p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-inverted btn-secondary narrow">Ver Produto</a>
                                </div>
                            </article><?php
                        endwhile;

                    } else { ?>
                        <p class="aviso-sem-produto">Nenhum produto encontrado!</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>