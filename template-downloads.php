<?php // Template Name: Downloads



get_header();



if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner"><span><?php the_title(); ?></span></h1>

                <h2 class="text-uppercase">Há 50 anos desenvolvendo as mais novas tecnologias do mercado.</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12">

                    <article class="artigo" id="sobre">

                        <?php the_content(); ?>

                    </article>



                    <ul class="file-list">

                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/20_.pdf">

                                Catálogo - Aricabos 2018

                            </a>

                        </li>

                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/12_.pdf">

                                Catálogo - CIMAF 2014

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/11_.pdf">

                                Blog do Engenheiro - A Trava do gancho é obrigatória ?

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/13_.pdf">

                                Check List de Inspeção em Cabos de Aço

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/14_.pdf">

                                Check List de Inspeção em Laços de Cabos de Aço

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/15_.pdf">

                                Check List de Inspeção em Cintas de Poliéster

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/16_.pdf">

                                Check List de Inspeção em Lingas de Corrente

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/17_.pdf">

                                Inspeção de Materiais para Elevação de Carga - Periodicidade

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/18_.jpg">

                                Guia para solicitação de Laços de Cabos de Aço

                            </a>

                        </li>



                        <li class="file-item">

                            <a href="#" data-filename="/downloads_files/19_.pdf">

                                Sinalização Manual para Equipamentos de Movimentação de Carga

                            </a>

                        </li>



                    </ul>

                </div>

            </div>

        </div>

    </section>

<?php endwhile; endif; ?>



    <!-- Modal -->

    <div class="modal fade" id="modal-downloads" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <h4 class="modal-title" id="myModalLabel">Baixar</h4>

                    <span class="filename"></span>

                </div>



                <form id="downloadsForm" action="<?php echo get_stylesheet_directory_uri(); ?>/inc/envia.php" method="POST">

                    <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">



                    <input type="hidden" name="tipo" value="download" />

                    <input type="hidden" name="downloaditem" value="">



                    <div class="modal-body">

                        <div class="form-group">

                            <label for="nome" class="sr-only">Nome:</label>

                            <input type="text" placeholder="Nome" name="nome" id="nome" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="email" class="sr-only">Email:</label>

                            <input type="email" placeholder="Email" name="email" id="email" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="telefone" class="sr-only">Telefone:</label>

                            <input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="form-control celular-input" >

                        </div>



                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancelar</button>

                        <button type="submit" class="btn btn-primary">Baixar!</button>

                    </div>



                </form>

            </div>

        </div>

    </div>



<?php get_footer(); ?>