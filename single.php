<?php get_header();

$options = get_option('page_1_section_option');



if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner <?php if(strlen(get_the_title()) > 39) echo ' small-font'; ?>"><span><?php the_title(); ?></span></h1>

                <h2 class="text-uppercase">Nossos produtos passam por rigorosos testes para apresentar a melhor qualidade para você</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-md-6">

                    <article class="artigo" id="sobre">

                        <?php the_content(); ?>

                        <div class="callToAction marginT62">

                            <button class="btn btn-primary inverted" data-toggle="modal" data-target="#modal-duvidas">Dúvidas? Nós te ligamos</button>

                        </div>

                    </article>

                </div>



                <aside id="sidebar-produto" class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">

                    <?php $get = unserialize( get_post_meta($post->ID, 'galeria', true ) );



                    if(!empty($get)):?>

                        <!-- Galeria -->

                        <div id="galeria" class="row marginT30">

                            <div class="big">

                                <?php

                                $cont = 0;



                                    foreach ($get as $key => $id_img):

                                        if($cont != 0){

                                            continue;

                                        }else {

                                            $fullImage = wp_get_attachment_image_src($id_img, 'full');

                                            $mainThumbImg = wp_get_attachment_image_src($id_img, 'galeria-produto');

                                            $mainAlt = get_post_meta($id_img, '_wp_attachment_image_alt');



                                            if (!empty($mainAlt)) : $alt = $mainAlt[0];

                                            else: $alt = 'Imagem sem texto alternativo';

                                            endif; ?>

                                            <img src="<?php echo $mainThumbImg[0]; ?>" id="img-zoom"

                                                 data-zoom-image="<?= $fullImage[0] ?>" title="<?php echo $alt; ?>"

                                                 alt="<?php echo $alt; ?>"/>

                                            <?php

                                            $cont++;

                                        }

                                    endforeach;

                                ?>

                            </div>

                            <div class="thumbs" id="thumb2">

                                <?php foreach($get as $key => $id_img):

                                    $thumbImg = wp_get_attachment_image_src( $id_img, 'thumb-galeria-produto' );

                                    $fullImage      = wp_get_attachment_image_src( $id_img, 'full' );

                                    $mainThumbImg   = wp_get_attachment_image_src( $id_img, 'galeria-produto' );?>

                                <a href="#" data-image="<?php echo $mainThumbImg[0]; ?>" data-zoom-image="<?= $fullImage[0] ?>">

                                    <img src="<?php echo $thumbImg[0]; ?>" id="img-zoom" class="thumb item">

                                </a>

                                <?php endforeach; ?>

                            </div>



                        </div>

                    <?php endif; ?>



                    <?php require('inc/form-catalogo.php'); ?>



                </aside>

            </div>





            <div class="hidden-xs">

                <div class="col-lg-12 col-md-12">

                    <h2 class="titulo">Tabela de Medidas</h2>

                    <p>Aqui você pode consultar qual o tamanho ideal para seu projeto, além da quantidade necessária para suas especificações de produtos</p>

                </div>

                <div class="col-lg-12 col-md-12">

                    <form action="<?php bloginfo('url'); ?>/orcamento/?acao=adicionar" method="post">

                        <div id="tabela-produtos">

                            <?php

                            $tabelas        = isset($options['tabelas']) ? $options['tabelas'] : array();

                            $selectedTable  = get_post_meta($post->ID, 'tabela_selecionada', true);

                            $variacoes      = get_post_meta($post->ID, 'variacoes', true);

                            $currentTable   = NULL;

                            $has_meters     = get_post_meta($post->ID, 'has_meters', true);?>

                            <table>

                                <thead>



                                <tr>

                                    <?php

                                    // Search table by slug

                                    foreach($tabelas as $chaveTable => $tabela):



                                        // When find the table

                                        if(('tabela_'.$tabela['id']) === $selectedTable):



                                            $currentTable = $tabela;



                                            echo '<td class="headcol"><span>Referência</span></td>';



                                            // Repeat campos

                                            foreach($tabela['campos'] as $chave => $campo) { ?>

                                                <td><?php echo $campo['commonName']; ?></td><?php

                                            }



                                            if($has_meters === 'yes')

                                                echo '<td>Metros</td>';



                                            echo '<td>Quantidade</td>';

                                        endif;

                                    endforeach; ?>

                                </tr>

                                </thead>



                                <tbody>

                                <?php if(count($variacoes) >= 1 && $currentTable != null):

                                    foreach($variacoes as $chave => $variacao): ?>

                                        <tr>

                                            <?php



                                            echo '<td class="headcol"><span>'.$variacao[0]["referencia"].'</span></td>';



                                            // Repeat campos

                                            foreach($currentTable['campos'] as $key => $campo) { ?>

                                                <td class="long"><?php echo $variacao[++$key][$campo['slug']]; ?></td><?php

                                            }

                                            if($has_meters === 'yes'):?>

                                                <td><input class="form-control" type="text" min="0" name="items[<?php echo $chave; ?>][metros]" value="0" placeholder="Metros..."/></td>

                                            <?php endif; ?>

                                            <td><input class="form-control" type="number" min="0" name="items[<?php echo $chave; ?>][quantidade]" value="0" placeholder="Quantidade..."/></td>

                                            <input type="hidden" name="items[<?php echo $chave; ?>][referencia]" value="<?php echo $variacao[0]['referencia']; ?>">

                                        </tr>

                                    <?php endforeach;

                                endif;?>

                                </tbody>

                            </table>

                            <input type="hidden" name="product_name" value="<?php the_title(); ?>">

                            <input type="hidden" name="product_id" value="<?php echo get_the_ID(); ?>">

                        </div>

                        <button type="submit" class="btn btn-secondary pull-right">Orçar</button>

                    </form>

                </div>

            </div>



        </div>

    </section>

<?php endwhile; endif; ?>



    <!-- Modal -->

    <div class="modal fade" id="modal-duvidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <h4 class="modal-title" id="myModalLabel">Dúvidas? Nós te ligamos!</h4>

                </div>

                <form id="callme" action="<?php echo get_stylesheet_directory_uri(); ?>/inc/envia.php" method="POST">

                    <input type="hidden" name="tipo" value="callme" />

                    <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">



                    <input type="hidden" name="produto" value="<?= the_title(); ?>">



                    <div class="modal-body">

                        <div class="form-group">

                            <label for="nome" class="sr-only">Nome:</label>

                            <input type="text" placeholder="Nome" name="nome" id="nome" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="email" class="sr-only">Email:</label>

                            <input type="mail" placeholder="Email" name="email" id="email" class="form-control" >

                        </div>



                        <div class="form-group">

                            <label for="telefone" class="sr-only">Telefone:</label>

                            <input type="tel" placeholder="Telefone" name="telefone" id="telefone" class="form-control celular-input" >

                        </div>



                        <div class="form-group">

                            <label for="mensagem" class="sr-only">Mensagem:</label>

                            <textarea name="mensagem" placeholder="Mensagem" id="mensagem" class="form-control"></textarea>

                        </div>



                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Me ligue!</button>

                    </div>



                </form>

            </div>

        </div>

    </div>

<?php get_footer(); ?>