<?php
// Template Name: Contato
get_header();
$options = get_option('page_1_section_option');

if(have_posts()): while(have_posts()): the_post(); ?>
    <section id="banner">
        <div class="produto">
            <div class="container">
                <h1 class="titulo-banner"><span><?php the_title(); ?></span></h1>
                <h2 class="text-uppercase">Nossa equipe é capacitada e especializada para tirar todas as suas dúvidas!</h2>
            </div>
        </div>
    </section>

    <section id="content">
        <?php if ( function_exists('yoast_breadcrumb') ){
            yoast_breadcrumb('<div class="breadcrumb">','</div>');
        } ?>

        <div class="container">
            <div class="col-lg-6 col-md-6">
                <article class="artigo" id="sobre">
                    <?php the_content(); ?>


                    <form id="page-contato" action="<?php echo get_stylesheet_directory_uri(); ?>/inc/envia.php">
                        <input type="hidden" name="tipo" value="pagina"/>
                        <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label>Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="celular-input form-control"/>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" id="email" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label>Assunto</label>
                            <input type="text" name="assunto" id="asssunto" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label>Departamento</label>
                            <select name="departamento" id="departamento" class="form-control">
                                <option selected disabled>Selecione...</option>
                                <option value="compras">Compras</option>
                                <option value="engenharia">Engenharia</option>
                                <option value="vendas">Vendas</option>
                                <option value="recursos_humanos">Recursos Humanos</option>
                                <option value="diretoria">Diretoria</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Mensagem</label>
                            <textarea name="mensagem" id="mensagem" class="form-control" rows="3"></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </article>
            </div>
            <div class="col-lg-6 col-md-6">
                <iframe class="map" width="100%" height="350" frameborder="0" style="border: 0;" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Av.+Henry+Ford,+2257,+Vila+Prudente,+S%C3%A3o+Paulo&amp;aq=0&amp;oq=Av.+Henry+Ford,+2257+-+Vila+Prudente&amp;sll=-22.546052,-48.635514&amp;sspn=8.365328,14.27124&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Henry+Ford,+2257+-+Vila+Prudente,+S%C3%A3o+Paulo,+03109-001&amp;t=m&amp;ll=-23.58098,-46.592159&amp;spn=0.014946,0.016265&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
            </div>
        </div>
    </section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>