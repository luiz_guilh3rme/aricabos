<div class="row">
    <h3 class="titulo noMarginT">Pesquisar</h3>
    <form action="<?php bloginfo('url'); ?>/blog/" method="get" name="frmCatalogo" class="marginT30 search-wrap">
        <input type="text" class="form-control" name="s" id="s" placeholder="Digite aqui..." />
        <button type="submit" class="btn-search pull-right"><i class="fa fa-search"></i></button>
    </form>
</div>
<?php
query_posts(
    array(
        'posts_per_page' => 4,
        'post_type'      => 'blog'
    ));

if (have_posts()) :?>
    <div class="row">
        <h3 class="titulo">Artigos recentes</h3>

        <?php while ( have_posts() ) : the_post(); ?>
            <div class="recente marginT30">
                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                <span class="date"><?php echo get_the_time('d').'/'.get_the_time('m').'/'.get_the_time('y'); ?></span>
                <span class="comments"><?php comments_number( 'Nenhum comentário', '1 Comentário', '% Comentários' ); ?> <i class="fa fa-comments"></i></span>
            </div>
        <?php endwhile; ?>

    </div>
<?php endif; wp_reset_query(); ?>

<div class="row marginT62">
    <h3 class="titulo">Aricabos nas redes sociais</h3>
    <div class="social-minibox">
        <i class="fa fa-facebook"></i>
        <span class="likes-number">35941</span>
        <span class="likes-label">Likes</span>
    </div>
    <div class="social-minibox">
        <i class="fa fa-twitter"></i>
        <span class="likes-number">18376</span>
        <span class="likes-label">Follows</span>
    </div>
    <div class="social-minibox">
        <i class="fa fa-google-plus"></i>
        <span class="likes-number">35491</span>
        <span class="likes-label">Plus Ones</span>
    </div>
    <div class="social-minibox">
        <i class="fa fa-rss"></i>
        <span class="likes-number">35941</span>
        <span class="likes-label">Subscribers</span>
    </div>
</div>
<?php
$blogCatId = get_category_by_slug('blog');

$args = array(
    'orderby'                   => 'name',
    'child_of'                  => $blogCatId->term_id,
    'order'                     => 'ASC',
    'number'                    => '4',
    'taxonomy'                  => 'category',
);

$categories = get_categories( $args ); ?>
<div class="row marginT62">
    <h3 class="titulo">Categorias</h3>
    <ul class="cat-list marginT30">
        <?php foreach($categories as $key => $category):
            $link = get_term_link(intval($category->term_id), 'category'); ?>
        <li>
            <a href="<?php echo $link; ?>">
                <div class="nix-checkbox checkbox">
                    <label for="cat1"><?php echo $category->name; ?></label>
                    <span class="number pull-right"><?php echo $category->count ?></span>
                </div>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>