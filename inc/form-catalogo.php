<?php
if(!is_single() && get_page_template_slug() != "template-page-gallery.php"): ?>
<aside id="sidebar-produto" class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 hidden-xs">
    <div id="catalogo" class="row">
        <h2 class="titulo">Baixe nosso catálogo</h2>
        <p>Se você tiver dúvida na especificação técnica de algum produto nosso ou querer resolver outras questões, entre em contato conosco.</p>
        <form action="<?= get_stylesheet_directory_uri(); ?>/inc/envia.php" method="post" id="frmCatalogo" class="marginT30">
            <input type="hidden" name="tipo" value="download_catalogo">
            <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

            <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" />
            <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" />
            <button type="submit" class="btn btn-secondary marginT30 pull-right">Baixar</button>
        </form>
    </div>
</aside>
<?php else: ?>
<div id="catalogo" class="row">
    <h2 class="titulo">Baixe nosso catálogo</h2>
    <p>Se você tiver dúvida na especificação técnica de algum produto nosso ou querer resolver outras questões, entre em contato conosco.</p>
    <form action="<?= get_stylesheet_directory_uri(); ?>/inc/envia.php" method="post" id="frmCatalogo">
        <input type="hidden" name="tipo" value="download_catalogo">
        <input type="hidden" name="url" value="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" />
        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" />
        <button type="submit" class="btn btn-secondary marginT30 pull-right">Baixar</button>
    </form>
</div>
<?php endif; ?>
