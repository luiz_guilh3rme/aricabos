<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'PHPMailer/PHPMailerAutoload.php';

if ( !empty($_POST) ) {
    $nome =  $_POST['nome'];
    $email = $_POST['email'];

    printf('nome: '.$nome);
    printf(' email: '.$email);

    $mail = new PHPMailer(true);

   
    //Tipo de documento
    $mail->isHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->setLanguage('pt', 'PHPMailer/language/phpmailer.lang-pt.php');
    $data = array();

    $mail->addAddress('contato@aricabos.com.br', 'Aricabos');
    // $mail->addAddress('luiz.guilherme@3xceler.com.br', 'Luiz');
    $mail->addCC('natasha@aricabos.com.br', 'Aricabos');

    // $newsp = new Newsp("74cf7ca587d20ff402c6d1fcff1283a0");

    // $newsp->saveLead($_POST, array('url', 'tipo'));

    $mail->Subject="Solicitação de newsletter - Aricabos";
    $body = '
    <body style="margin:0; padding:0;">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
    <tr height="120" align="center" style="background-color:#3B345E;">
    <td valign="middle"><img src="../img/aricabos-logo.png" height="34" alt="Aricabos"></td>
    </tr>
    </table>
    <br/>
    <table width="100%" align="left" cellpadding="0" cellspacing="0">
    <tr align="center">
    <table width="680" cellpadding="10" cellspacing="0" style="border-top:1px solid #eee; border-left:1px solid #eee; border-right:1px solid #eee;">
    <tr>
    <td width="65" height=""align="left" style=" font-family:\'Georgia\'; font-size:14px; border-bottom:1px solid #eee; border-right:1px solid #eee; font-weight:bold;">Nome</td>
    <td style="border-bottom:1px solid #eee;">'. $nome .'</td>
    </tr>
    <tr>
    <td width="65" height=""align="left" style=" font-family:\'Georgia\'; font-size:14px; border-bottom:1px solid #eee; border-right:1px solid #eee; font-weight:bold;">Email</td>
    <td style="border-bottom:1px solid #eee;">'. $email .'</td>
    </tr>
    </table>
    </td>
    </tr>
    </table>';

    $mail->Body = $body;

    if ($mail->send()) {
        echo 'Mensagem enviada com sucesso!';
    }  else {
        echo 'Erro o enviar a mensagem: ' . $mail->ErrorInfo;
    }

}