<?php get_header(); ?>

    <section id="banner">
        <div class="produto">
            <div class="container">
                <h1 class="titulo-banner"><span>Página não encontrada!</span></h1>
                <h2 class="text-uppercase">Há mais de 40 anos desenvolvendo as mais novas tecnologias do mercado.</h2>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div>
                        <br>
                        
                        Ops, parece que está página não existe!
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>