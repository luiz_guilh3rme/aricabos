<?php get_header(); ?>
<section id="banner">
    <div class="produto">
        <div class="container">
            <h1 class="titulo-banner"><span>Nossos Produtos</span></h1>
            <h2>LOREM IPSUM CARDOGMAS PROFECSIONAL THE</h2>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="col-lg-6 col-md-6">
            <article class="artigo" id="sobre">
                <h2 class="titulo">Todos os Produtos</h2>
                <p class="paragrafo">Proin sagittis feugiat elit finibus pretium. Donec et tortor non purus vulputate tincidunt. Cras congue posuer eros eget egestas. Aenean varius ex ut ex laoreet fermentum. Curabitur ornare varius mi, sit amet faucibus erate.</p>
                <p class="paragrafo">Consectetur id. Aenean sit amet massa eu velit commodo cursus fringilla a tellus. Morbi eros urna, mollis porta feugiat non, ornare eu augue. Sed rhoncus est sit amet diam tempus, et tristique est viverra. Etiam ex tellus, sectur at dapibus id, luctus at odio. Proin mattis congue tristiqueconsectetur id. Aenean sit amet massa eu velit commodo cursus fringilla a tellus. Morbi eros urna.</p>
            </article>
        </div>
        <aside id="sidebar-produto" class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1">
            <div id="catalogo" class="row">
                <h2 class="titulo">Baixe nosso catálogo</h2>
                <p>Proin sagittis feugiat elit finibus pretium. Donec et tortor non purus vulputate tincidunt. Cras congue posuer eros eget egestas. Aenean variu</p>
                <form method="post" name="frmCatalogo" class="marginT30">
                    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" />
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" />
                    <button type="submit" class="btn btn-secondary marginT30 pull-right">Baixar</button>
                </form>
            </div>
        </aside>

        <div class="col-lg-12 col-md-12">
            <?php for($x=0;$x<=23;$x++){?>
                <article class="aplicacao col-lg-4 col-md-4">
                    <div class="blue-icon"><i class="fa fa-lightbulb-o"></i></div>
                    <div class="apli-info">
                        <h3>Correntes de Aço</h3>
                        <p>Proin sagittis feugiat elit finibus pretium. Donec et tortor non purus vulputate tincidunt. Cras congue posuer.</p>
                        <button class="btn btn-secondary narrow">Ver Todos</button>
                    </div>
                </article>
            <?php } ?>
        </div>
    </div>
</section>
