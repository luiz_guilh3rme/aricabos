<?php get_header(); ?>
    <section id="banner">
        <div class="blog">
            <div class="container">
                <h1 class="titulo-blog"><span>Blog</span></h1>
                <h2>Aqui nossa criatividade aflora!</h2>
            </div>
        </div>
    </section>

    <?php if ( function_exists('yoast_breadcrumb') ){
        yoast_breadcrumb('<div class="breadcrumb">','</div>');
    } ?>

    <section id="blog" class="container marginT62">
        <div class="col-lg-8 col-md-8">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
            <div class="row post">
                <aside id="social-blog" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                    <div class="post-social">
                        <div class="date"><?php the_time('d'); ?><br> <?php the_time('M'); ?></div>
                        <div class="like">LIKE</div>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </aside>

                <div id="blog-content" class="col-lg-10 col-md-10 col-sm-10">
                    <article>
                        <?php if(has_post_thumbnail()) the_post_thumbnail('post-thumb', array('class'=>' img-responsive thumb')); ?>
                        <h4 class="titulo-blog"><?php the_title(); ?></h4>
                        <div class="extra">
                            <i class="fa fa-tags"></i> <span>Tag 1, Tag 2</span>
                            <i class="fa fa-comments marginL20"></i> <span><?php comments_number( 'Nenhum comentário', '1 Comentário', '% Comentários' ); ?></span>
                        </div>
                        <?php the_content(); ?>
                    </article>
                </div>
            </div>
            <?php endwhile; wp_reset_postdata(); else: ?>
                <p>Pagina não encontrada. :(</p>
            <?php endif; ?>
        </div>

        <aside id="blog-sidebar" class="col-lg-4 col-md-4">
            <?php require 'inc/sidebar-blog.php'; ?>
        </aside>
    </section>
<?php get_footer(); ?>