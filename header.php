<?php
ob_start();
global $title;

if(!isset($_SESSION)) {
    ini_set('session.save_path', '/tmp');
    session_start();
}
if(!isset($_SESSION['orcamento'])) {$_SESSION['orcamento'] = array();} ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title><?php if(empty($title)) wp_title(); else echo $title; ?></title>
    <link rel="icon"
          type="image/x-icon"
          href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">

    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.min.css">

    <?php wp_head(); ?>
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WRNWNJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WRNWNJ');</script>
<!-- End Google Tag Manager -->

<div id="perspective" class="perspective effect-airbnb">
    <div class="main-container">
        <div class="wrapper"><!-- wrapper needed for scroll -->
            <header>
                <div id="top-info" class="hidden-xs hidden-sm">
                    <div class="container">
                        <div class="col-lg-4 col-md-4">
                            <div class="inline-block icon <?php if(is_home()): echo 'hidden-md hidden-sm hidden-xs'; else: echo 'hidden-md hidden-sm hidden-xs'; endif; ?>"><i class="fa fa-map-marker gigant light"></i></div>

                            <div class="inline-block">
                                <span class="light medium uppercase">Av. Henry Ford n° 2257</span><br>
                            <span class="light regular">Parque da Mooca - CEP 03109-001 - São Paulo / SP
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="inline-block icon hidden-md hidden-sm hidden-xs"><i class="fa fa-phone gigant light"></i></div>

                            <div class="inline-block">
                                <span class="light small uppercase">SÃO PAULO -  </span><a class="tel-sp
" href="tel:+551126027222"><span class="light small grandesp">11 2602 7222</span></a><br>
                                <span class="light small uppercase">OUTRAS regiões - </span><a class="tel-outras-regioes" href="tel:0800-777-2226"><span class="light small outraslocalidades">0800 777 2226</span></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="inline-block icon hidden-md hidden-sm hidden-xs">
                                <i class="fa fa-clock-o gigant light"></i>
                            </div>

                            <div class="inline-block light">
                                SEGUNDA - SEXTA<br>
                                Das 8:00 am às 18:00 pm
                            </div>

                        </div>
                        <div id="social" class="col-lg-2 col-md-2 hidden-xs">
                            <a href="https://www.facebook.com/pages/Ari-Cabos-Ind%C3%BAstria-Com%C3%A9rcio-Importa%C3%A7%C3%A3o-e-Exporta%C3%A7%C3%A3o-Ltda/240100239448071" target="_blank"><i class="fa fa-facebook light"></i></a>
                            <a href="https://twitter.com/aricabos" target="_blank"><i class="fa fa-twitter light"></i></a>
                            <a href="http://br.linkedin.com/pub/ari-cabos/62/a51/477" target="_blank"><i class="fa fa-linkedin light"></i></a>
                        </div>
                    </div>
                </div>

                <nav id="top-menu" class="navbar navbar-default">
                    <div id="menu-wrap" class="container">
                        <div id="desktop-menu-wrap">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" id="showMenu">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="<?php echo site_url(); ?>" class="navbar-brand">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/aricabos-logo.png" alt="Ari cabos" title="Ari cabos" />
                                </a>
                            </div>

                            <!-- link app -->
                            <div class="link-app hidden-sm hidden-xs">
                                <p>BAIXE O APLICATIVO GRATUITO</p>
                                <div class="links">
                                    <a class="app-store" href="https://appsto.re/br/ERVBcb.i" target="_blank">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/app-store.png" alt="App Ari cabos" title="App Ari cabos" />
                                    </a>
                                    <a class="app-play" href="https://play.google.com/store/apps/details?id=br.com.app.gpu31225.gpufdf2607edb6be3a4d404770770d82d73" target="_blank">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google-play.png" alt="App Ari cabos" title="App Ari cabos" />
                                    </a>
                                </div>
                            </div>
                            <!-- //link app -->

                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="/orcamento/">
                                            <?php $quant = count($_SESSION['orcamento']);
                                            if($quant > 0):?>
                                                <span class="cart-count"><?php echo $quant; ?></span>
                                            <?php endif; ?>
                                            <i class="icon-cart"></i>
                                        </a>
                                    </li>
                                    <li><a class="toggle-search" href="#"><i class="icon-search"></i></a></li>
                                </ul>

                                <?php
                                wp_nav_menu( array(
                                        'menu'              => 'primary',
                                        'theme_location'    => 'primary',
                                        'depth'             => 2,
                                        'container'         => '',
                                        'container_class'   => '',
                                        'container_id'      => '',
                                        'menu_class'        => 'nav navbar-nav navbar-right',
                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                        'walker'            => new wp_bootstrap_navwalker())
                                );?>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown sub-menu-columns">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos</a>
                                        <ul class="dropdown-menu ">
                                            <?php

                                            $blogCatId = get_category_by_slug('blog');

                                            $args = array(
                                                'orderby'                   => 'name',
                                                'parent'                    => 0,
                                                'exclude'                   => $blogCatId->term_id,
                                                'order'                     => 'ASC',
                                                'taxonomy'                  => 'category',
                                            );

                                            $categories = get_categories( $args );
                                            foreach($categories as $category): ?>
                                                <li>
                                                    <a href="<?= get_category_link($category->term_id); ?>"><?= $category->name ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="search-wrap" class="container">
                            <form action="<?php bloginfo('url'); ?>" method="get">
                                <div class="form-group">
                                    <input type="search" placeholder="Digite o que procura..." name="s" class="search-input"/>
                                    <input type="hidden" name="cat" value="<?php $blog = get_category_by_slug('blog'); echo '-'. $blog->term_id; ?>"/>
                                </div>
                            </form>
                            <span class="toggle-search icon-search"></span>
                        </div>
                    </div>
                </nav>
            </header>

            <!-- modal Mobile -->
            <div class="modal-app hidden-md hidden-lg">
                <div class="modal-content">
                    <div class="close-modal">+</div>
                    <div class="link-app">
                        <p>BAIXE O NOSSO <br/> APLICATIVO GRATUITO</p>
                        <div class="links">
                            <a href="https://appsto.re/br/ERVBcb.i" target="_blank">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/app-store-2.png" alt="App Ari cabos" title="App Ari cabos" />
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=br.com.app.gpu31225.gpufdf2607edb6be3a4d404770770d82d73" target="_blank">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/google-play.png" alt="App Ari cabos" title="App Ari cabos" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // modal Mobile -->

