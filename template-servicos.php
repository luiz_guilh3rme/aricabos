<?php // Template Name: Serviços

get_header();



global $post;



$servicos = new WP_Query(array(

    'post_type' => 'page',

    'parent' => 0,

    'posts_per_page' => -1,

    'order'         => 'ASC',

    'post_parent'    => $post->ID



));





if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner"><span><?php the_title(); ?></span></h1>

                <h2 class="text-uppercase">Há 50 anos desenvolvendo as mais novas tecnologias do mercado.</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12">

                    <article class="artigo" id="sobre">

                        <?php the_content(); ?>

                    </article>

                </div>

            </div>



            <div class="row">

                <?php if($servicos->have_posts()): while($servicos->have_posts()): $servicos->the_post(); ?>

                    <div class="col-sm-3">

                        <div class="service-item">

                            <div class="service-thumbnail">

                                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">

                                <?php

                                if(has_post_thumbnail()):

                                    the_post_thumbnail('service-thumb',array('class'=> ' img-responsive '));

                                else: ?>

                                    <img src="<?= get_stylesheet_directory_uri() ?>/css/img/cog.jpg" class="img-responsive" width="265" height="265" alt="<?php the_title() ?>">

                                <?php endif; ?>

                                </a>

                            </div>



                            <a href="<?php the_permalink() ?>" class="service-title"><?php the_title() ?></a>

                        </div>



                    </div>

                <?php endwhile; endif; ?>

            </div>

        </div>

    </section>

<?php endwhile; endif; ?>



<?php get_footer(); ?>