$(function () {
    var viewPort = $(window).width();

    $("#sync1").owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: false,
        pagination:false,
        responsiveRefreshRate : 200
    });
    var owl = $("#sync1").data('owlCarousel');

    var thumbnails = $("#sync2 .item");
    thumbnails.on('click', function (e) {
        var index = thumbnails.index(e.currentTarget);
        thumbnails.removeClass('active');
        $(this).addClass('active');
        owl.goTo(index);
    });

    $('#sync2').owlCarousel({
        items: 4,
        pagination: false,
        navigation: true
    });

    $("#clientes-carousel").owlCarousel({
        items : 4,
        itemsCustom : false,
        itemsTablet: [768,2],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
        singleItem : false,
        itemsScaleUp : false,
        navigation : false,
        scrollPerPage : false,
        pagination : false,
        paginationNumbers: false,
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        stopOnHover : true

    });

    $(".arrow-down").click(function(a){
        a.preventDefault();
        var scroll = $("#content").offset().top;
        $("html, body").animate({
            scrollTop: scroll
        }, 1000);
    });

    $('.toggle-search').on('click', function () {
       $('#menu-wrap').toggleClass('active');
    });

    if(viewPort <= '768'){
        // Responsive Links
        $('#lista-produtos li a').each(function (key, produto) {
            $(produto).attr('href', $(produto).data('responsive-link')).removeAttr('data-toggle');
        })
    }


    // Change Adwords Calltracker
    _googWcmGet('grandesp', '11-2602-7222');
    _googWcmGet('outraslocalidades', '11-0800-777-2226');

});
