$(function () {



    // Add Methods

    jQuery.validator.addMethod("testEmail", function (value, element) {

        return this.optional(element) || /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/i.test(value);

    }, "Digite e-mail valido.");



    var SPMaskBehavior = function (val) {

            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';

        },

        spOptions = {

            onKeyPress: function(val, e, field, options) {

                field.mask(SPMaskBehavior.apply({}, arguments), options);

            }

        };



    $('.celular-input').mask(SPMaskBehavior, spOptions);



    $('#page-contato').validate({

        rules: {

            nome: {

                required: true,

                minlength: 2

            },

            email: {

                required: true,

                email: true,

                testEmail: true

            },

            mensagem: {

                required: true,

                minlength: 10

            }

        },messages: {

            nome: {

                required: "Digite seu nome.",

                minlength: "Digite seu nome completo."

            },

            email: {

                required: "Digite um email.",

                email: "Digite e-mail valido."

            },



            mensagem: {

                required: "Digite uma mensagem.",

                minlength: "A mensagem deve ter no mínimo 10 caracteres."

            }

        },submitHandler: function( form ){



            var dados = $( form ).serialize();

            var host =  form.action;

            $.ajax({

                type: "POST",

                url: host,

                async: true,

                data: dados,

                success: function( data ){

                    var result;

                    if(data == '') result = data; else result = JSON.parse(data);



                    if(result.status == '200'){

                        window.location ="/obrigado/";

                    }else{

                        console.error('Erro: '+ data);

                    }

                },

                error: function(data, textStatus){

                    console.error('ERRORS: ' + textStatus);

                }

            });



            return false;

        }

    });



    $('#callme').validate({

        rules: {

            nome: {

                required: true,

                minlength: 2

            },

            email: {

                required: true,

                email: true,

                testEmail: true

            },

            telefone: {

                required: true

            }

        },messages: {

            nome: {

                required: "Digite seu nome.",

                minlength: "Digite seu nome completo."

            },

            email: {

                required: "Digite um email.",

                email: "Digite e-mail valido."

            },

            telefone: {

                required: "Digite um número de telefone."

            }

        },submitHandler: function( form ){



            var dados = $( form ).serialize();

            var host =  form.action;

            $.ajax({

                type: "POST",

                url: host,

                async: true,

                data: dados,

                success: function( data ){

                    var result;

                    if(data == '') result = data; else result = JSON.parse(data);



                    if(result.status == '200'){

                        window.location ="/obrigado/";

                    }else{

                        console.error('Erro: '+ data);

                    }

                },

                error: function(data, textStatus){

                    console.error('ERRORS: ' + textStatus);

                }

            });



            return false;

        }

    });



    $('#orcamento').validate({

        rules: {

            nome: {

                required: true,

                minlength: 2

            },

            email: {

                required: true,

                email: true,

                testEmail: true

            },

            telefone: {

                required: true

            },

            mensagem: {

                required: true,

                minlength: 10

            }

        },messages: {

            nome: {

                required: "Digite seu nome.",

                minlength: "Digite seu nome completo."

            },

            email: {

                required: "Digite um email.",

                email: "Digite e-mail valido."

            },

            telefone: {

                required: "Digite um telefone.",

                email: "Digite telefone valido."

            },

            mensagem: {

                required: "Digite uma mensagem.",

                minlength: "A mensagem deve ter no mínimo 10 caracteres."

            }

        },submitHandler: function( form ){

            $(form).find('button[type="submit"]').attr('disabled', true);

            $(form).find('button[data-dismiss="modal"]').hide();



            var dados = $( form ).serialize();

            var host =  form.action;

            $.ajax({

                type: "POST",

                url: host,

                async: true,

                data: dados,

                success: function( data ){

                    var result;



                    console.log(data);



                    if(data == '')result = data;

                    else result = JSON.parse(data);



                    if(result.status == '200'){

                        window.location ="/obrigado/";

                    }else{

                        alert(result.message);



                        // Reabilita ações

                        $(form).find('button[type="submit"]').attr('disabled', false);

                        $(form).find('button[data-dismiss="modal"]').show();



                    }

                },

                error: function(data, textStatus){

                    console.log('ERRORS: ' + textStatus);





                    // Reabilita ações

                    $(form).find('button[type="submit"]').attr('disabled', false);

                    $(form).find('button[data-dismiss="modal"]').show();

                }

            });



            return false;

        }

    });



    $('#frmCatalogo').validate({

        rules: {

            nome: {

                required: true,

                minlength: 2

            },

            email: {

                required: true,

                email: true,

                testEmail: true

            }

        },messages: {

            nome: {

                required: "Digite seu nome.",

                minlength: "Digite seu nome completo."

            },

            email: {

                required: "Digite um email.",

                email: "Digite e-mail valido."

            }

        },submitHandler: function( form ){

            $(form).find('button[type="submit"]').attr('disabled', true);

            $(form).find('button[data-dismiss="modal"]').hide();



            var dados = $( form ).serialize();

            var host =  form.action;

            $.ajax({

                type: "POST",

                url: host,

                async: true,

                dataType: 'json',

                data: dados,

                success: function( result ){



                    if(result.status == '200'){

                        window.location ="/baixar-catalogo/";

                    }else{

                        alert(result.message);



                        // Reabilita ações

                        $(form).find('button[type="submit"]').attr('disabled', false);

                        $(form).find('button[data-dismiss="modal"]').show();



                    }

                },

                error: function(data, textStatus){

                    console.log('ERRORS: ' + textStatus);



                    // Reabilita ações

                    $(form).find('button[type="submit"]').attr('disabled', false);

                    $(form).find('button[data-dismiss="modal"]').show();

                }

            });



            return false;

        }

    });

});