<?php get_header();



if(have_posts()): while(have_posts()): the_post(); ?>

    <section id="banner">

        <div class="produto">

            <div class="container">

                <h1 class="titulo-banner"><span><?php the_title(); ?></span></h1>

                <h2 class="text-uppercase">Há 50 anos desenvolvendo as mais novas tecnologias do mercado.</h2>

            </div>

        </div>

    </section>



    <section id="content">

        <?php if ( function_exists('yoast_breadcrumb') ){

            yoast_breadcrumb('<div class="breadcrumb">','</div>');

        } ?>



        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12">

                    <article class="artigo" id="sobre">

                        <?php the_content(); ?>

                    </article>

                </div>

            </div>

        </div>

    </section>

<?php endwhile; endif; ?>





<?php get_footer(); ?>