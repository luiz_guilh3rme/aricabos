<?php
/* Pagina Diferenciada. */
status_header( 200 );
$title = 'Aplicações | '. get_bloginfo('name');
add_action('wp_head', 'add_head');
function add_head (){
    echo '<meta name="description" content="Confira todas as aplicações em que os produtos da Aricabos otimizam tarefas como amarrar, içar e movimentar cargas. Faça seu Orçamento!" />';
}


get_header();
$tags = get_terms('post_tag', array('hide_empty'=> false));
?>
    <section id="banner">
        <div class="produto">
            <div class="container">
                <h1 class="titulo-banner"><span>Aplicações</span></h1>
                <h2 class="text-uppercase">Produtos de alta qualidade e com o melhor desempenho em vários segmentos</h2>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="col-lg-6 col-md-6">
                <article class="artigo" id="sobre">
                    <p class="paragrafo">Conheça os produtos Aricabos categorizado por suas respectivas aplicações.</p>
                </article>
            </div>
            <?php require('inc/form-catalogo.php'); ?>

            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <?php
                    if ($tags) {
                        foreach ($tags as $tag) {
                            $attchId    = get_option('tag_'.$tag->term_id);
                            $attchUrl   = wp_get_attachment_image_src( $attchId['attachment_id'], 'full' );

                            $id         = $tag ->term_id;
                            $title      = $tag->name;

                            ob_start();
                            echo $tag->description;
                            $old_content = ob_get_clean();
                            $new_content = strip_tags($old_content);
                            $content_excerpt = substr($new_content, 0 ,88).'...'; ?>
                            <article class="aplicacao col-lg-4 col-md-4">
                            <div class="blue-icon"><img src="<?php echo $attchUrl[0] ?>"/></div>
                            <div class="apli-info">
                                <h3><?= $title; ?></h3>
                                <p>
                                    <?php echo $content_excerpt ?>
                                </p>
                            </div>
                            <a href="<?= get_tag_link( $id ) ?>" class="btn-inhome">Ver produtos</a>
                            </article><?php
                        }
                    } else { ?>
                        <p class="aviso-sem-produto">Nenhum produto encontrado!</p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>