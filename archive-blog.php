<?php get_header();
$paged  = ( get_query_var('page') ) ? get_query_var('page') : 1;

$args   = array(
    'post_type' => 'blog',
    'posts_per_page' => 4,
    'paged' => $paged
);
if(is_category()){
    $cat = get_query_var('cat');
    $args['cat'] = $cat;
}
$theposts  = new WP_Query($args); ?>

    <section id="banner">
        <div class="blog">
            <div class="container">
                <h1 class="titulo-blog"><span>Blog</span></h1>
                <h2>Aqui nossa criatividade aflora!</h2>
            </div>
        </div>
    </section>

    <?php if ( function_exists('yoast_breadcrumb') ){
        yoast_breadcrumb('<div class="breadcrumb">','</div>');
    } ?>

    <section id="blog" class="container marginT62">
        <div class="col-lg-8 col-md-8">
            <?php if ($theposts->have_posts()) : ?>
            <div class="post_list">
                <?php while ($theposts->have_posts()) : $theposts->the_post(); ?>
                    <div class="row post">
                        <aside id="social-blog" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                            <div class="post-social">
                                <div class="date"><?php the_time('d'); ?><br> <?php the_time('M'); ?></div>
                                <div class="like">LIKE</div>
                                <div class="icons">
                                    <a target="_blank" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php the_permalink() ?>&p[images][0]=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>&p[title]=<?php the_title() ?>&p[summary]=<?php the_excerpt() ?>"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="https://twitter.com/share?url=<?php the_permalink() ?>&text=<?php the_title() ?>"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" href="https://pinterest.com/pin/create/bookmarklet/?media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>&url=<?php the_permalink() ?>&description=<?php the_title() ?>"><i class="fa fa-pinterest"></i></a>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php the_permalink() ?>"><i class="fa fa-google-plus"></i></a>
                                    <a target="_blank" href="http://www.linkedin.com/shareArticle?url=<?php the_permalink() ?>&title=<?php the_title() ?>"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </aside>

                        <div id="blog-content" class="col-lg-10 col-md-10 col-sm-10">
                            <article>
                                <?php if(has_post_thumbnail()) the_post_thumbnail('post-thumb', array('class'=>' img-responsive thumb')); ?>
                                <a href="<?php the_permalink(); ?>"><h4 class="titulo-blog"><?php the_title(); ?></h4></a>
                                <div class="extra">
                                    <i class="fa fa-comments marginL20"></i> <span><?php comments_number( 'Nenhum comentário', '1 Comentário', '% Comentários' ); ?></span>
                                </div>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink(); ?>" class="more">Leia mais <i class="fa fa-long-arrow-right"></i></a>
                            </article>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php
                if (function_exists(custom_pagination)) :
                    custom_pagination($theposts->max_num_pages,"",$paged, 'col-lg-offset-2');
                endif;
            else:
                echo _e('Desculpe, não encontramos nenhum post.');
            endif; ?>
        </div>

        <aside id="blog-sidebar" class="col-lg-4 col-md-4">
            <?php require 'inc/sidebar-blog.php'; ?>
        </aside>
    </section>

<?php get_footer(); ?>